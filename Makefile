mod_replace:
	go mod edit -replace bitbucket.org/cornjacket/iot=/home/david/work/go/src/bitbucket.org/cornjacket/iot
	go mod edit -replace bitbucket.org/cornjacket/airtrafficcontrol=/home/david/work/go/src/bitbucket.org/cornjacket/airtrafficcontrol
	go mod edit -replace bitbucket.org/cornjacket/view_hndlr=/home/david/work/go/src/bitbucket.org/cornjacket/view_hndlr
mod_drop_replace:
	go mod edit -dropreplace bitbucket.org/cornjacket/iot
	go mod edit -dropreplace bitbucket.org/cornjacket/airtrafficcontrol
	go mod edit -dropreplace bitbucket.org/cornjacket/view_hndlr
docker_local_bash:
	docker exec -it local_event_processor /bin/ash
docker_run_prod:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	# I believe if I run this docker on the host network, then the container will be able to connect with the database
	docker run -it --name=local_event_processor --env="DB_PASSWORD=abc" -p 8081:8081 cornjacket/iot_app_4_event_processor
docker_run_dev:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	# I believe if I run this docker on the host network, then the container will be able to connect with the database
	docker run -it --name=local_event_processor -p 8081:8081 event_processor_dev 
docker_build_prod:
	docker build . -f ./docker/Dockerfile.prod -t cornjacket/iot_app_4_event_processor
docker_build_dev:
	docker build . -f ./docker/Dockerfile.dev -t event_processor_dev
mysql_run:
	docker run --rm --detach --name=my-mariadb --env="MYSQL_ROOT_PASSWORD=abc" --publish 3306:3306 --volume=/root/docker/my-mariadb/conf.d:/etc/mysql/conf.d --volume=/storage/docker/mariadb-data:/var/lib/mysql mariadb
mysql_stop:
	docker stop my-mariadb
mysql_client:
	mysql -h localhost -P 3306 --protocol=tcp -u root -p
kafka_up:
	docker-compose -f ./local_test/kafka/docker-compose.yaml up
kafka_down:
	docker-compose -f ./local_test/kafka/docker-compose.yaml down
aws_tag:
	docker tag cornjacket/iot_app_4_event_processor 923799911166.dkr.ecr.us-west-1.amazonaws.com/iot_app_4_event_processor
aws_login:
	aws ecr get-login-password --region us-west-1 | docker login --username AWS --password-stdin 923799911166.dkr.ecr.us-west-1.amazonaws.com
aws_push:
	docker push 923799911166.dkr.ecr.us-west-1.amazonaws.com/iot_app_4_event_processor
consul_start_local:
	consul agent -dev -bind=127.0.0.1 -node=localhost
