package event 

/* 

   TODO(drt) - rewrite this using the event_processor client

   this is a quick test of the event_processor using the cmd_hndkr client lib. but changing the port to 8081

   a better way is to use the event_processor client_lib.

*/



import (
	"fmt"
	"log"
	"testing"

	. "gopkg.in/check.v1"

	cmd_client "bitbucket.org/cornjacket/cmd_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"
	//"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc" // DRT removed for testing
)

var pktCmdClient cmd_client.PacketCmdHndlrClient
var tRespCmdClient cmd_client.TallocRespCmdHndlrClient

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
}

var _ = Suite(&TestSuite{})

// TODO(drt): Need a way to pass the cmdHost name to the test program so that it can test locally or in production
//var cmdHost = "localhost"
//var cmdHost = "54.153.99.75"
//var cmdHost = "ec2-54-177-124-20.us-west-1.compute.amazonaws.com"
var cmdHost = "54.183.168.154" // testing event processor by using cmdHndlr client. This works because it uses the /packet interface if i chane port to 8081.

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")

	// time delay to guarantee so that the server is up and talking to the database
	//time.Sleep(1 * time.Second)
	// Initialize Packet Client
	if err := pktCmdClient.Open(); err != nil {
		log.Fatal("pktCmdClient.Open() error: ", err)
	}
	// Initialize TallocResp Client
	// TODO: ENV must be set in order for the cmdHost name to be specified. This file is outdated.
	if err := tRespCmdClient.Open(); err != nil {
		log.Fatal("tRespCmdClient.Open() error: ", err)
	}

}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
	// TODO(drt) - I could add a test interface to drop/add tables to clear the previous test data
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")

	// TODO(drt) - close the db connection

}

// The following test sends a cloudsync packet and then flushes with 60 packets to dummy euis 
func (s *TestSuite) TestRxCloudSyncPacket123cPath(c *C) {
	fmt.Printf("system_test.packetTest\n")

	// Send CloudSync Packet
	eui := "000000000000123c"
	ts1 := uint64(1529525468005)
	p := NewRxPacketWithData(eui, ts1, "ff03000228ffff23ea58b9")

	var err error
	err = pktCmdClient.SendPacket(p)
	c.Assert(err, Equals, nil)
	fmt.Printf("system_test.CloudSyncRxPacket\n")

}

// TODO(DRT) - These function below belongs in a system test library to be used across application
// TODO(DRT) - Further parameterize this function so that it can add other key information
func NewCloudSyncRxPacket(eui string, ts uint64) message.UpPacket {
	data := "ff00060004ffff00030000"
	return NewRxPacketWithData(eui, ts, data)
}

func NewRxPacketWithData(eui string, ts uint64, data string) message.UpPacket {
	return message.UpPacket{
		Dr:   "SF10 BW125 4/5",
		Ts:   ts,
		Eui:  eui,
		Ack:  false,
		Cmd:  "rx",
		Snr:  13.3,
		Data: data,
		Fcnt: 1115,
		Freq: uint64(90230000),
		Port: 1,
		Rssi: -88,
		}
}

