module bitbucket.org/cornjacket/event_processor

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol v0.1.12
	bitbucket.org/cornjacket/cmd_hndlr v0.1.15
	bitbucket.org/cornjacket/discovery v0.1.0
	bitbucket.org/cornjacket/iot v0.1.9
	bitbucket.org/cornjacket/view_hndlr v0.1.14
	github.com/go-openapi/runtime v0.19.24
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/segmentio/kafka-go v0.4.8
	github.com/streadway/amqp v1.0.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
)
