#!/bin/bash

docker run --detach --name=my-mariadb --env="MYSQL_ROOT_PASSWORD=abc" --publish 3306:3306 --volume=/root/docker/my-mariadb/conf.d:/etc/mysql/conf.d --volume=/storage/docker/mysql-data:/var/lib/mysql mariadb

