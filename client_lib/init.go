package client_lib

import (
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type TransportType int

const (
	Disabled TransportType = 0 // default
	HttpPost TransportType = 1
	Kafka    TransportType = 2
	RabbitMQ TransportType = 3
)

// I don't think that the app should configure the client. The client should self-configure
// by reading from the environment. The event_hndlr client should look for a very specific set of
// environment variables. This makes the client even more usable. This follows the pattern of other
// client libraries that I have seen which directly grab from the environment instead of depending on the
// user application to extract from the environment. But for now I am just going to get the RabbitMQ
// data path working prior to revisiting and changing the interface.

// if RabbitMQ or Kafka Url is given, then client behaves in Loosely-coupled mode using message queues
// for packet data flow. Otherwise client behaves in tightly-coupled mode using http Post.
type EventHndlrClientConfig struct {
	PacketTransportType     TransportType // supports Kafka, RabbitMQ, and defaults to HTTP
	TallocRespTransportType TransportType // only support HTTP
	BrokerAddress           string        // rabbitMQ address
	KafkaHostname           string
	KafkaPort               string
	Hostname                string // specifies http_post endpoint
	Port                    string // specifies http_post port
	//PacketPath          string // used as path for http_post
	//TallocRespPath      string // used as path for http post
}

type EventHandlrClientInterface interface {
	Open() error
	SendPacket(packet message.UpPacket) error
	SendTallocResp(tallocresp tallocrespproc.TallocResp) error
}
