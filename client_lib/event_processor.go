package client_lib

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	kafka "github.com/segmentio/kafka-go"
	"github.com/streadway/amqp" // rabbitMQ

	"bitbucket.org/cornjacket/discovery"
	"bitbucket.org/cornjacket/event_processor/app/utils/env"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type EventHndlrClient struct {
	Config      EventHndlrClientConfig
	KafkaWriter *kafka.Writer
	Channel     *amqp.Channel
	Queue       amqp.Queue
}

// new open function which determines transport type by itself
// for now this function will self detect what type of egress interface to use Kafka, RabbitMQ, or Http
// later I can let the caller force a specific interface for local testing if needed.
// first check for Kafka environment variables
// then check for RabbitMQ
// fallback to Http
func (s *EventHndlrClient) Open() error {
	s.Config.TallocRespTransportType = HttpPost // later can add Kafka or RabbitMQ

	brokerAddr := os.Getenv("EVENT_PROCESSOR_RABBITMQ_BROKER_ADDR")
	kafkaHostname := os.Getenv("EVENT_PROCESSOR_KAFKA_HOSTNAME")

	if len(brokerAddr) != 0 {
		s.Config.PacketTransportType = RabbitMQ
		s.Config.BrokerAddress = brokerAddr
	} else if len(kafkaHostname) != 0 {
		s.Config.PacketTransportType = Kafka
		s.Config.KafkaHostname = kafkaHostname
		s.Config.KafkaPort = env.GetVar("EVENT_PROCESSOR_KAFKA_PORT", "9092", false)
	} else { // default
		s.Config.PacketTransportType = HttpPost
		if discovery.UrlProvided() {
			s.Config.Hostname, s.Config.Port = discovery.LookupService("event-processor")
		} else {
			s.Config.Hostname = env.GetVar("EVENT_PROCESSOR_HOSTNAME", "localhost", false)
			s.Config.Port = env.GetVar("EVENT_PROCESSOR_PORT", "8081", false)
		}
	}
	// TODO: What is default Kafka address http://localhost:9092
	//if len(brokerAddr) == 0 {
	//	brokerAddr = "amqp://guest:guest@localhost:5672/"
	//}

	switch s.Config.PacketTransportType {

	case RabbitMQ:

		var conn *amqp.Connection
		var err error
		var dialComplete = false
		var channelComplete = false

		for i := 0; i < 15; i++ {

			if !dialComplete {
				conn, err = amqp.Dial(s.Config.BrokerAddress)
				if err == nil {
					dialComplete = true //return err
				}

			}
			if dialComplete && !channelComplete {
				s.Channel, err = conn.Channel()
				//defer ch.Close() -- TODO(drt) - note that there is no retry mechanism
				if err == nil {
					channelComplete = true //return err
				}
			}
			if channelComplete {
				s.Queue, err = s.Channel.QueueDeclare(
					"event-processor-packet", // name
					true,                 // durable
					false,                // delete when unused
					false,                // exclusive
					false,                // no-wait
					nil,                  // arguments
				)
				if err == nil {
					break
				}
			}
			log.Println("event_processor client: RabbitMQ retry. error: ", err)
			time.Sleep(time.Duration(3) * time.Second)
		}
		if err != nil {
			log.Println("event_processor client: RabbitMQ failure. error: ", err)
		} else {
			log.Println("event_processor client: RabbitMQ init succeeded.")
		}

		return err

	case Kafka:

		kafkaURL := s.Config.KafkaHostname + ":" + s.Config.KafkaPort
		topic := "cmd_event_packet"
		log.Printf("eventHndlrClient: kafka open %s, %s\n", kafkaURL, topic)
		s.KafkaWriter = getKafkaWriter(kafkaURL, topic)
		// TODO(DRT) where does the kafka writer get closed? Is there a deferral that can be done somewhere
		// Maybe there needs to be an app.Close function that gets deferred that closes kafka as well as the DB
		//defer kafkaWriter.Close()
		return nil

	default: // HttpPost

		return s.Ping()

	}
}

func getKafkaWriter(kafkaURL, topic string) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Brokers:  []string{kafkaURL},
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
	})
}

// Ping makes multiple attempts to see if the external service is up before giving up and returning an error.
func (s *EventHndlrClient) Ping() error {
	// check if enabled
	// if so, then make a GET call to root of (). We could change this to /ping
	// if there is an error, then repeatedly call for X times with linear backoff until success
	// ping loop
	var pingErr error
	for i := 0; i < 15; i++ {
		host := s.URL("")
		fmt.Printf("Pinging %s\n", host)
		_, pingErr = http.Get(host)
		if pingErr != nil {
			fmt.Println(pingErr)
		} else {
			fmt.Println("Ping replied.")
			break
		}
		time.Sleep(time.Duration(3) * time.Second)
	}
	if pingErr != nil {
		log.Println("This is the error:", pingErr)
	}
	return pingErr
}

func (s *EventHndlrClient) SendPacket(packet message.UpPacket) error {

	var err error

	switch s.Config.PacketTransportType {
	case RabbitMQ:
		log.Printf("eventHndlrClient: Sending packet via RabbitMQ.\n") // TODO: Add a desination string here
		reqBodyBytes := new(bytes.Buffer)
		json.NewEncoder(reqBodyBytes).Encode(packet)
		err = s.Channel.Publish(
			"",           // exchange
			s.Queue.Name, // routing key
			false,        // mandatory
			false,        // immediate
			amqp.Publishing{
				ContentType: "application/json",
				Body:        reqBodyBytes.Bytes(),
			})
		if err != nil {
			log.Printf("eventHndlrClient: publish packet to RabbitMQ error: %s\n", (err.Error()))
		} else {
			log.Printf(" [x] Sent %s", reqBodyBytes)
		}

	case Kafka:
		log.Printf("eventHndlrClient: Sending packet via Kafka: %s\n", s.Config.KafkaHostname+":"+s.Config.KafkaPort)
		ctx := context.TODO() // TODO(DRT): should i add req.context later passed in from the worker pool
		reqBodyBytes := new(bytes.Buffer)
		json.NewEncoder(reqBodyBytes).Encode(packet)
		msg := kafka.Message{
			Key:   []byte(fmt.Sprintf("address-%s", "test")), //req.RemoteAddr)),
			Value: reqBodyBytes.Bytes(),                      //body,
		}
		err = s.KafkaWriter.WriteMessages(ctx, msg) // TODO(DRT): for now an empty ctx until i found out how to use ths
		if err != nil {
			log.Printf("eventHndlrClient: Send packet to Kafka error: %s\n", (err.Error()))
		}

	case HttpPost:
		url := s.URL("/packet")
		log.Printf("PacketEventHndlrClient: Sending packet to %s\n", url)
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)
		err = Post(url, b)

	default:
		log.Printf("PacketEventHndlrClient is disabled. Dropping packet.\n")
	}

	return err

}

// currently only support transport over HttpPort
func (s *EventHndlrClient) SendTallocResp(tallocresp tallocrespproc.TallocResp) error {

	var err error
	switch s.Config.TallocRespTransportType {
	case HttpPost:
		url := s.URL("/tallocresp")
		log.Printf("TallocRespEventHndlrClient: Sending tallocresp to %s via HttpPost\n", url)
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(tallocresp)
		err = Post(url, b)
	default:
		log.Printf("TallocRespEventHndlrClient is disabled. Dropping packet.\n")
	}

	return err

}

func (s *EventHndlrClient) URL(path string) string {
	return "http://" + s.Config.Hostname + ":" + s.Config.Port + path
}

// post will return error on non 200 or 201 status code
func Post(url string, b *bytes.Buffer) error {
	var res *http.Response
	var err error
	res, err = http.Post(url, "application/json; charset=utf-8", b) // Should I defer close of the res.Body
	if err == nil {
		//log.Printf("http_post nil: statusCode: %d\n", res.StatusCode)
		if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusCreated {
			body, _ := ioutil.ReadAll(res.Body)
			err = errors.New(string(body))
		}
	}
	return err
}
