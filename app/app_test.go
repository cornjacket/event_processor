package app

import (
	"fmt"
	"log"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/event_processor/app/controllers"
	"bitbucket.org/cornjacket/event_processor/app/mocks"
	event_client "bitbucket.org/cornjacket/event_processor/client_lib"
	"bitbucket.org/cornjacket/iot/message"
)

var client event_client.EventHndlrClient

func NewAppServiceMock() AppService {
	a := AppService{}
	a.Context = NewAppContextMock()
	return a
}

func NewAppContextMock() *controllers.AppContext {
	context := controllers.AppContext{}
	context.ViewHndlrClient = &mocks.ViewHndlrClientMock{}                 // implements ViewHandlrPostInterface
	context.AirTrafficControlService = &mocks.AirTrafficControlClientMock{} // implements AirTrafficControlPostInterface
	return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
	AppService AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppServiceMock()
	s.AppService.InitEnv()
	s.AppService.DropTables()
	s.AppService.SetupDatabase()
	go s.AppService.Run()
	if err := client.Open(); err != nil {
		log.Fatal("client.Open() error: ", err)
	}
}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
	s.AppService.CreateTables()
	//if err := s.AppService.Context.AirTrafficControlService.Open("localhost", "8082", "/packet", "/talloc", numRetries); err != nil {
	if err := s.AppService.Context.AirTrafficControlService.Open(); err != nil {
		log.Fatal("AirTrafficControlService.Open() error: ", err)
	}
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
	s.AppService.DropTables()
	s.AppService.Context.ViewHndlrClient = &mocks.ViewHndlrClientMock{} // assigning a new ContextMock resets the local mock storage
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

func sendOnboardPacket(c *C, eui string) {
	// Send Packet
	fmt.Printf("sendOnboardPacket\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  eui,
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "FF00060004ffff00030000", // this is an onboard packet
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	// This is needed so that the TestTearDown func doesn't drop the tables before the event_processor is finished processing
	time.Sleep(100 * time.Millisecond)

	c.Assert(mocks.ViewLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.ViewLastRxPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastRxPacket.Cmd, Equals, p.Cmd)

	c.Assert(mocks.ViewLastNodeState.LastPktRcvd, Equals, p.Data)
	c.Assert(mocks.ViewLastNodeState.Eui, Equals, p.Eui)

	c.Assert(mocks.ViewLastNodeState.SysApi, Equals, 6)
	c.Assert(mocks.ViewLastNodeState.AppApi, Equals, 3)
	c.Assert(mocks.ViewLastNodeState.AppNum, Equals, 4)
}

func (s *TestSuite) TestPacketPath(c *C) {
	// Send Packet
	fmt.Printf("packetTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "12345678",
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	// This is needed so that the TestTearDown func doesn't drop the tables before the event_processor is finished processing
	time.Sleep(100 * time.Millisecond)

	c.Assert(mocks.ViewLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.ViewLastRxPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastNodeState.LastPktRcvd, Equals, p.Data)
	c.Assert(mocks.ViewLastNodeState.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastNodeState.SysApi, Equals, 0) // These fields haven't been set because no onboard packet
	c.Assert(mocks.ViewLastNodeState.AppApi, Equals, 0)
	c.Assert(mocks.ViewLastNodeState.AppNum, Equals, 0)

}

func (s *TestSuite) TestOnboardPacketPath(c *C) {
	// Send Packet
	fmt.Printf("onboardPacketTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "FF00060004ffff00030000", // this is an onboard packet
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	// This is needed so that the TestTearDown func doesn't drop the tables before the event_processor is finished processing
	time.Sleep(100 * time.Millisecond)

	c.Assert(mocks.ViewLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.ViewLastRxPacket.Eui, Equals, p.Eui)

	c.Assert(mocks.ViewLastNodeState.LastPktRcvd, Equals, p.Data)
	c.Assert(mocks.ViewLastNodeState.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastNodeState.SysApi, Equals, 6)
	c.Assert(mocks.ViewLastNodeState.AppApi, Equals, 3)
	c.Assert(mocks.ViewLastNodeState.AppNum, Equals, 4)

	c.Assert(mocks.AtcLastTallocReq.NodeEui, Equals, p.Eui)
	c.Assert(mocks.AtcLastTallocReq.SysApi, Equals, 6)
	c.Assert(mocks.AtcLastTallocReq.AppApi, Equals, 3)
	c.Assert(mocks.AtcLastTallocReq.AppNum, Equals, 4)
	c.Assert(mocks.AtcLastTallocReq.TimeReq, Equals, 15)

}

func (s *TestSuite) TestStatusPacketPath(c *C) {
	eui := "1234"
	sendOnboardPacket(c, eui) // The onboard packet needs to proceed the status packet so that the controller can understand the opcode

	// Send Packet
	fmt.Printf("statusPacketTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  eui,
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "0A03060004ffff00030080", // this is an app status packet
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	// This is needed so that the TestTearDown func doesn't drop the tables before the event_processor is finished processing
	time.Sleep(100 * time.Millisecond)

	c.Assert(mocks.ViewLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.ViewLastRxPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastNodeState.LastPktRcvd, Equals, p.Data)
	c.Assert(mocks.ViewLastNodeState.Eui, Equals, p.Eui)
	c.Assert(mocks.ViewLastNodeState.ActualStateA, Equals, 1)
	c.Assert(mocks.ViewLastNodeState.ActualStateB, Equals, 1)
	c.Assert(mocks.ViewLastData.ValueA, Equals, 1)
	c.Assert(mocks.ViewLastData.ValueB, Equals, 1)

}
