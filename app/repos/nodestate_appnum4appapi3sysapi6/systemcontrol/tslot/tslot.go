package tslot

import (
  "strconv"
)

type TSlot struct {
  value int
  updated bool
}

func New() *TSlot {
  return &TSlot{}
}

func (x *TSlot) IsUpdated() bool {
  return x.updated
}

func (x *TSlot) Get() int {
  return x.value
}

func (x *TSlot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *TSlot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *TSlot) String() string {
  return "TSlot"
}

func (x *TSlot) Type() string {
  return "int"
}

func (x *TSlot) Modifiers() string {
  return "not null"
}

func (x *TSlot) QueryValue() string {
  return strconv.Itoa(x.value)
}

