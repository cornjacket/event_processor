package globaldownstreamenable

import (
  "strconv"
)

type GlobalDownstreamEnable struct {
  value int
  updated bool
}

func New() *GlobalDownstreamEnable {
  return &GlobalDownstreamEnable{}
}

func (x *GlobalDownstreamEnable) IsUpdated() bool {
  return x.updated
}

func (x *GlobalDownstreamEnable) Get() int {
  return x.value
}

func (x *GlobalDownstreamEnable) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *GlobalDownstreamEnable) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *GlobalDownstreamEnable) String() string {
  return "GlobalDownstreamEnable"
}

func (x *GlobalDownstreamEnable) Type() string {
  return "tinyint"
}

func (x *GlobalDownstreamEnable) Modifiers() string {
  return "not null"
}

func (x *GlobalDownstreamEnable) QueryValue() string {
  return strconv.Itoa(x.value)
}

