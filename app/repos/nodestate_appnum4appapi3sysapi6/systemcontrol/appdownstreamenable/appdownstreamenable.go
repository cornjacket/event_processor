package appdownstreamenable

import (
  "strconv"
)

type AppDownstreamEnable struct {
  value int
  updated bool
}

func New() *AppDownstreamEnable {
  return &AppDownstreamEnable{}
}

func (x *AppDownstreamEnable) IsUpdated() bool {
  return x.updated
}

func (x *AppDownstreamEnable) Get() int {
  return x.value
}

func (x *AppDownstreamEnable) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AppDownstreamEnable) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AppDownstreamEnable) String() string {
  return "AppDownstreamEnable"
}

func (x *AppDownstreamEnable) Type() string {
  return "tinyint"
}

func (x *AppDownstreamEnable) Modifiers() string {
  return "not null"
}

func (x *AppDownstreamEnable) QueryValue() string {
  return strconv.Itoa(x.value)
}

