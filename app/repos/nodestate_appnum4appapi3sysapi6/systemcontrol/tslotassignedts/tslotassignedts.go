package tslotassignedts

import (
  "strconv"
)

type TSlotAssignedTs struct {
  value int64
  updated bool
}

func New() *TSlotAssignedTs {
  return &TSlotAssignedTs{}
}

func (x *TSlotAssignedTs) IsUpdated() bool {
  return x.updated
}

func (x *TSlotAssignedTs) Get() int64 {
  return x.value
}

func (x *TSlotAssignedTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *TSlotAssignedTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *TSlotAssignedTs) String() string {
  return "TSlotAssignedTs"
}

func (x *TSlotAssignedTs) Type() string {
  return "bigint"
}

func (x *TSlotAssignedTs) Modifiers() string {
  return "not null"
}

func (x *TSlotAssignedTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

