package updatedat

import (
  "strconv"
  "time"
)

type UpdatedAt struct {
  value time.Time
  updated bool
}

func New() *UpdatedAt {
  return &UpdatedAt{}
}

func (x *UpdatedAt) IsUpdated() bool {
  return x.updated
}

func (x *UpdatedAt) Get() time.Time {
  return x.value
}

func (x *UpdatedAt) Set(value time.Time) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *UpdatedAt) Load(value time.Time) {
  x.value = value
  x.updated = false
}

func (x *UpdatedAt) String() string {
  return "UpdatedAt"
}

func (x *UpdatedAt) Type() string {
  return "datetime"
}

func (x *UpdatedAt) Modifiers() string {
  return "not null default current_timestamp on update current_timestamp"
}

func (x *UpdatedAt) QueryValue() string {
  return strconv.FormatInt(x.value.UnixNano(), 10)
}

