package eui

import (
)

type Eui struct {
  value string
  updated bool
}

func New() *Eui {
  return &Eui{}
}

func (x *Eui) IsUpdated() bool {
  return x.updated
}

func (x *Eui) Get() string {
  return x.value
}

func (x *Eui) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Eui) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *Eui) String() string {
  return "Eui"
}

func (x *Eui) Type() string {
  return "varchar(22)"
}

func (x *Eui) Modifiers() string {
  return "not null"
}

func (x *Eui) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

