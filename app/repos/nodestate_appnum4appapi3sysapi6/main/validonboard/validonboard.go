package validonboard

import (
  "strconv"
)

type ValidOnboard struct {
  value int
  updated bool
}

func New() *ValidOnboard {
  return &ValidOnboard{}
}

func (x *ValidOnboard) IsUpdated() bool {
  return x.updated
}

func (x *ValidOnboard) Get() int {
  return x.value
}

func (x *ValidOnboard) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ValidOnboard) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ValidOnboard) String() string {
  return "ValidOnboard"
}

func (x *ValidOnboard) Type() string {
  return "tinyint"
}

func (x *ValidOnboard) Modifiers() string {
  return "not null"
}

func (x *ValidOnboard) QueryValue() string {
  return strconv.Itoa(x.value)
}

