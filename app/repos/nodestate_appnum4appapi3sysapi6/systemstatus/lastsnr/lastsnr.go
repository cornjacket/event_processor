package lastsnr

import (
  "strconv"
)

type LastSnr struct {
  value int
  updated bool
}

func New() *LastSnr {
  return &LastSnr{}
}

func (x *LastSnr) IsUpdated() bool {
  return x.updated
}

func (x *LastSnr) Get() int {
  return x.value
}

func (x *LastSnr) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastSnr) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *LastSnr) String() string {
  return "LastSnr"
}

func (x *LastSnr) Type() string {
  return "int"
}

func (x *LastSnr) Modifiers() string {
  return "not null"
}

func (x *LastSnr) QueryValue() string {
  return strconv.Itoa(x.value)
}

