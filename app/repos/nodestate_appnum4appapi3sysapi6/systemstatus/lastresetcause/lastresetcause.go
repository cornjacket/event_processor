package lastresetcause

import (
  "strconv"
)

type LastResetCause struct {
  value int
  updated bool
}

func New() *LastResetCause {
  return &LastResetCause{}
}

func (x *LastResetCause) IsUpdated() bool {
  return x.updated
}

func (x *LastResetCause) Get() int {
  return x.value
}

func (x *LastResetCause) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastResetCause) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *LastResetCause) String() string {
  return "LastResetCause"
}

func (x *LastResetCause) Type() string {
  return "int"
}

func (x *LastResetCause) Modifiers() string {
  return "not null"
}

func (x *LastResetCause) QueryValue() string {
  return strconv.Itoa(x.value)
}

