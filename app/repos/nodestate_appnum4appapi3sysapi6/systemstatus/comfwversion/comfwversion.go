package comfwversion

import (
  "strconv"
)

type ComFwVersion struct {
  value int
  updated bool
}

func New() *ComFwVersion {
  return &ComFwVersion{}
}

func (x *ComFwVersion) IsUpdated() bool {
  return x.updated
}

func (x *ComFwVersion) Get() int {
  return x.value
}

func (x *ComFwVersion) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ComFwVersion) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ComFwVersion) String() string {
  return "ComFwVersion"
}

func (x *ComFwVersion) Type() string {
  return "int"
}

func (x *ComFwVersion) Modifiers() string {
  return "not null"
}

func (x *ComFwVersion) QueryValue() string {
  return strconv.Itoa(x.value)
}

