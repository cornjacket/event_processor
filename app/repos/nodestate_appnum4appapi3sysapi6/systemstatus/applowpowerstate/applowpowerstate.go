package applowpowerstate

import (
  "strconv"
)

type AppLowPowerState struct {
  value int
  updated bool
}

func New() *AppLowPowerState {
  return &AppLowPowerState{}
}

func (x *AppLowPowerState) IsUpdated() bool {
  return x.updated
}

func (x *AppLowPowerState) Get() int {
  return x.value
}

func (x *AppLowPowerState) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AppLowPowerState) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AppLowPowerState) String() string {
  return "AppLowPowerState"
}

func (x *AppLowPowerState) Type() string {
  return "tinyint"
}

func (x *AppLowPowerState) Modifiers() string {
  return "not null"
}

func (x *AppLowPowerState) QueryValue() string {
  return strconv.Itoa(x.value)
}

