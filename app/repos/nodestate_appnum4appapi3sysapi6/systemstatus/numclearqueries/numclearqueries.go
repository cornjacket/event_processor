package numclearqueries

import (
  "strconv"
)

type NumClearQueries struct {
  value int
  updated bool
}

func New() *NumClearQueries {
  return &NumClearQueries{}
}

func (x *NumClearQueries) IsUpdated() bool {
  return x.updated
}

func (x *NumClearQueries) Get() int {
  return x.value
}

func (x *NumClearQueries) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *NumClearQueries) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *NumClearQueries) String() string {
  return "NumClearQueries"
}

func (x *NumClearQueries) Type() string {
  return "int"
}

func (x *NumClearQueries) Modifiers() string {
  return "not null"
}

func (x *NumClearQueries) QueryValue() string {
  return strconv.Itoa(x.value)
}

