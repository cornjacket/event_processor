package appfwversion

import (
  "strconv"
)

type AppFwVersion struct {
  value int
  updated bool
}

func New() *AppFwVersion {
  return &AppFwVersion{}
}

func (x *AppFwVersion) IsUpdated() bool {
  return x.updated
}

func (x *AppFwVersion) Get() int {
  return x.value
}

func (x *AppFwVersion) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AppFwVersion) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AppFwVersion) String() string {
  return "AppFwVersion"
}

func (x *AppFwVersion) Type() string {
  return "int"
}

func (x *AppFwVersion) Modifiers() string {
  return "not null"
}

func (x *AppFwVersion) QueryValue() string {
  return strconv.Itoa(x.value)
}

