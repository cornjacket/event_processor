package lastresetcausets

import (
  "strconv"
)

type LastResetCauseTs struct {
  value int64
  updated bool
}

func New() *LastResetCauseTs {
  return &LastResetCauseTs{}
}

func (x *LastResetCauseTs) IsUpdated() bool {
  return x.updated
}

func (x *LastResetCauseTs) Get() int64 {
  return x.value
}

func (x *LastResetCauseTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastResetCauseTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastResetCauseTs) String() string {
  return "LastResetCauseTs"
}

func (x *LastResetCauseTs) Type() string {
  return "bigint"
}

func (x *LastResetCauseTs) Modifiers() string {
  return "not null"
}

func (x *LastResetCauseTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

