package nodestate_appnum4appapi3sysapi6

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/main/eui"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/main/validonboard"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/main/createdat"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/main/updatedat"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/appnum"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/appapi"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/sysapi"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/appfwversion"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/sysfwversion"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/comapi"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/comfwversion"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastpktrcvd"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastpktrcvdts"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastvoltagercvd"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastvoltagercvdts"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastresetcause"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastresetcausets"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastrssi"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastrssits"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastsnr"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/lastsnrts"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/numdownstreams"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/numclearqueries"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemstatus/applowpowerstate"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/globaldownstreamenable"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/systemdownstreamenable"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/appdownstreamenable"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/tslot"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/tslotvalid"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/systemcontrol/tslotassignedts"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/appstatus/actualstatea"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/appstatus/actualstateb"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/appcontrol/expectedstatea"
  "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6/appcontrol/expectedstateb"
)

type Row struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  AppNum	int	`json:"appnum"`
  AppApi	int	`json:"appapi"`
  SysApi	int	`json:"sysapi"`
  AppFwVersion	int	`json:"appfwversion"`
  SysFwVersion	int	`json:"sysfwversion"`
  ComApi	int	`json:"comapi"`
  ComFwVersion	int	`json:"comfwversion"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktRcvdTs	int64	`json:"lastpktrcvdts"`
  LastVoltageRcvd	int	`json:"lastvoltagercvd"`
  LastVoltageRcvdTs	int64	`json:"lastvoltagercvdts"`
  LastResetCause	int	`json:"lastresetcause"`
  LastResetCauseTs	int64	`json:"lastresetcausets"`
  LastRssi	int	`json:"lastrssi"`
  LastRssiTs	int64	`json:"lastrssits"`
  LastSnr	int	`json:"lastsnr"`
  LastSnrTs	int64	`json:"lastsnrts"`
  NumDownstreams	int	`json:"numdownstreams"`
  NumClearQueries	int	`json:"numclearqueries"`
  AppLowPowerState	int	`json:"applowpowerstate"`
  GlobalDownstreamEnable	int	`json:"globaldownstreamenable"`
  SystemDownstreamEnable	int	`json:"systemdownstreamenable"`
  AppDownstreamEnable	int	`json:"appdownstreamenable"`
  TSlot	int	`json:"tslot"`
  TSlotValid	int	`json:"tslotvalid"`
  TSlotAssignedTs	int64	`json:"tslotassignedts"`
  ActualStateA	int	`json:"actualstatea"`
  ActualStateB	int	`json:"actualstateb"`
  ExpectedStateA	int	`json:"expectedstatea"`
  ExpectedStateB	int	`json:"expectedstateb"`
}

type PrevRow struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  AppNum	int	`json:"appnum"`
  AppApi	int	`json:"appapi"`
  SysApi	int	`json:"sysapi"`
  AppFwVersion	int	`json:"appfwversion"`
  SysFwVersion	int	`json:"sysfwversion"`
  ComApi	int	`json:"comapi"`
  ComFwVersion	int	`json:"comfwversion"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktRcvdTs	int64	`json:"lastpktrcvdts"`
  LastVoltageRcvd	int	`json:"lastvoltagercvd"`
  LastVoltageRcvdTs	int64	`json:"lastvoltagercvdts"`
  LastResetCause	int	`json:"lastresetcause"`
  LastResetCauseTs	int64	`json:"lastresetcausets"`
  LastRssi	int	`json:"lastrssi"`
  LastRssiTs	int64	`json:"lastrssits"`
  LastSnr	int	`json:"lastsnr"`
  LastSnrTs	int64	`json:"lastsnrts"`
  NumDownstreams	int	`json:"numdownstreams"`
  NumClearQueries	int	`json:"numclearqueries"`
  AppLowPowerState	int	`json:"applowpowerstate"`
  GlobalDownstreamEnable	int	`json:"globaldownstreamenable"`
  SystemDownstreamEnable	int	`json:"systemdownstreamenable"`
  AppDownstreamEnable	int	`json:"appdownstreamenable"`
  TSlot	int	`json:"tslot"`
  TSlotValid	int	`json:"tslotvalid"`
  TSlotAssignedTs	int64	`json:"tslotassignedts"`
  ActualStateA	int	`json:"actualstatea"`
  ActualStateB	int	`json:"actualstateb"`
  ExpectedStateA	int	`json:"expectedstatea"`
  ExpectedStateB	int	`json:"expectedstateb"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("NodeState_Appnum4Appapi3Sysapi6.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("NodeState_Appnum4Appapi3Sysapi6.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  nodestate_appnum4appapi3sysapi6 := New()
  //currentTable := nodestate_appnum4appapi3sysapi6.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, nodestate_appnum4appapi3sysapi6.CreateTableString())
  // Create table if it doesn't already exist
  err := nodestate_appnum4appapi3sysapi6.CreateTable()
  if err != nil {
    fmt.Printf("nodestate_appnum4appapi3sysapi6.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("nodestate_appnum4appapi3sysapi6.CreateTable() succeeded!!\n")
  }
}

type NodeState_Appnum4Appapi3Sysapi6 struct {
  db           *sql.DB
  tx           *sql.Tx // used for the transaction
  txValid      bool // determines whether a transaction is currently in progress
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Eui	*eui.Eui
  ValidOnboard	*validonboard.ValidOnboard
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  AppNum	*appnum.AppNum
  AppApi	*appapi.AppApi
  SysApi	*sysapi.SysApi
  AppFwVersion	*appfwversion.AppFwVersion
  SysFwVersion	*sysfwversion.SysFwVersion
  ComApi	*comapi.ComApi
  ComFwVersion	*comfwversion.ComFwVersion
  LastPktRcvd	*lastpktrcvd.LastPktRcvd
  LastPktRcvdTs	*lastpktrcvdts.LastPktRcvdTs
  LastVoltageRcvd	*lastvoltagercvd.LastVoltageRcvd
  LastVoltageRcvdTs	*lastvoltagercvdts.LastVoltageRcvdTs
  LastResetCause	*lastresetcause.LastResetCause
  LastResetCauseTs	*lastresetcausets.LastResetCauseTs
  LastRssi	*lastrssi.LastRssi
  LastRssiTs	*lastrssits.LastRssiTs
  LastSnr	*lastsnr.LastSnr
  LastSnrTs	*lastsnrts.LastSnrTs
  NumDownstreams	*numdownstreams.NumDownstreams
  NumClearQueries	*numclearqueries.NumClearQueries
  AppLowPowerState	*applowpowerstate.AppLowPowerState
  GlobalDownstreamEnable	*globaldownstreamenable.GlobalDownstreamEnable
  SystemDownstreamEnable	*systemdownstreamenable.SystemDownstreamEnable
  AppDownstreamEnable	*appdownstreamenable.AppDownstreamEnable
  TSlot	*tslot.TSlot
  TSlotValid	*tslotvalid.TSlotValid
  TSlotAssignedTs	*tslotassignedts.TSlotAssignedTs
  ActualStateA	*actualstatea.ActualStateA
  ActualStateB	*actualstateb.ActualStateB
  ExpectedStateA	*expectedstatea.ExpectedStateA
  ExpectedStateB	*expectedstateb.ExpectedStateB
}

func New() *NodeState_Appnum4Appapi3Sysapi6 {
  x := &NodeState_Appnum4Appapi3Sysapi6{}
  x.db = db_conn
  x.txValid = false
  x.tableName = "nodestate_appnum4appapi3sysapi6"
  x.prevVersion = 2
  x.version = 3
  x.Eui=	eui.New()
  x.ValidOnboard=	validonboard.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.AppNum=	appnum.New()
  x.AppApi=	appapi.New()
  x.SysApi=	sysapi.New()
  x.AppFwVersion=	appfwversion.New()
  x.SysFwVersion=	sysfwversion.New()
  x.ComApi=	comapi.New()
  x.ComFwVersion=	comfwversion.New()
  x.LastPktRcvd=	lastpktrcvd.New()
  x.LastPktRcvdTs=	lastpktrcvdts.New()
  x.LastVoltageRcvd=	lastvoltagercvd.New()
  x.LastVoltageRcvdTs=	lastvoltagercvdts.New()
  x.LastResetCause=	lastresetcause.New()
  x.LastResetCauseTs=	lastresetcausets.New()
  x.LastRssi=	lastrssi.New()
  x.LastRssiTs=	lastrssits.New()
  x.LastSnr=	lastsnr.New()
  x.LastSnrTs=	lastsnrts.New()
  x.NumDownstreams=	numdownstreams.New()
  x.NumClearQueries=	numclearqueries.New()
  x.AppLowPowerState=	applowpowerstate.New()
  x.GlobalDownstreamEnable=	globaldownstreamenable.New()
  x.SystemDownstreamEnable=	systemdownstreamenable.New()
  x.AppDownstreamEnable=	appdownstreamenable.New()
  x.TSlot=	tslot.New()
  x.TSlotValid=	tslotvalid.New()
  x.TSlotAssignedTs=	tslotassignedts.New()
  x.ActualStateA=	actualstatea.New()
  x.ActualStateB=	actualstateb.New()
  x.ExpectedStateA=	expectedstatea.New()
  x.ExpectedStateB=	expectedstateb.New()
  x.constraints = "constraint pk_example primary key (eui)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

// TODO(DRT): The following function should be implemented in guild
// Purpose: Return a Row struct with the current values of the data structure
func (x *NodeState_Appnum4Appapi3Sysapi6) GetRow() Row {
  return Row{
  	Eui: x.Eui.Get(),
	ValidOnboard: x.ValidOnboard.Get(),
	CreatedAt: x.CreatedAt.Get(),
	UpdatedAt: x.UpdatedAt.Get(),
	AppNum: x.AppNum.Get(),
	AppApi: x.AppApi.Get(),
	SysApi: x.SysApi.Get(),
	AppFwVersion: x.AppFwVersion.Get(),
	SysFwVersion: x.SysFwVersion.Get(),
	ComApi: x.ComApi.Get(),
	ComFwVersion: x.ComFwVersion.Get(),
	LastPktRcvd: x.LastPktRcvd.Get(),
	LastPktRcvdTs: x.LastPktRcvdTs.Get(),
	LastVoltageRcvd: x.LastVoltageRcvd.Get(),
	LastVoltageRcvdTs: x.LastVoltageRcvdTs.Get(),
	LastResetCause: x.LastResetCause.Get(),
	LastResetCauseTs: x.LastResetCauseTs.Get(),
	LastRssi: x.LastRssi.Get(),
	LastRssiTs: x.LastRssiTs.Get(),
	LastSnr: x.LastSnr.Get(),
	LastSnrTs: x.LastSnrTs.Get(),
	NumDownstreams: x.NumDownstreams.Get(),
	NumClearQueries: x.NumClearQueries.Get(),
	AppLowPowerState: x.AppLowPowerState.Get(),
	GlobalDownstreamEnable: x.GlobalDownstreamEnable.Get(),
	SystemDownstreamEnable: x.SystemDownstreamEnable.Get(),
	AppDownstreamEnable: x.AppDownstreamEnable.Get(),
	TSlot: x.TSlot.Get(),
	TSlotValid: x.TSlotValid.Get(),
	TSlotAssignedTs: x.TSlotAssignedTs.Get(),
	ActualStateA: x.ActualStateA.Get(),
	ActualStateB: x.ActualStateB.Get(),
	ExpectedStateA: x.ExpectedStateA.Get(),
	ExpectedStateB: x.ExpectedStateB.Get(),
	}

}



func PrevTableExists() bool {
  return prevTableExists;
}

// Close() should be called after all DB operations are done. If a transaction is currently in progress, then Close will Rollback or Commit based on the DB error
// sent to Close(). If Close or Rollback has already been called by the user, then calling Close() should have no effect.
func (x *NodeState_Appnum4Appapi3Sysapi6) Close (err error) (error, bool) {
  if x.txValid {
    if err != nil {
      return x.tx.Rollback(), true
    } else {
      return x.tx.Commit(), true
    }
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call rollback, releasing the lock
func (x *NodeState_Appnum4Appapi3Sysapi6) Rollback () (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Rollback(), true
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call commit, releasing the lock
func (x *NodeState_Appnum4Appapi3Sysapi6) Commit() (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Commit(), true
  }
  return nil, false
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetByEui (eui string, createIfNotFound bool, lockRow bool) (error, bool) {
  //fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui, lockRow)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    //fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  //fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      //fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        //fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        //fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
    // TODO: If lockRow, then SelectFromCurrent (with lock on) should be called again since now the row has been created... otherwise there is a potential race condition
  }
  return err, rcvd
}

func (x *NodeState_Appnum4Appapi3Sysapi6) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *NodeState_Appnum4Appapi3Sysapi6) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *NodeState_Appnum4Appapi3Sysapi6) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *NodeState_Appnum4Appapi3Sysapi6) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.ValidOnboard.Load(row.ValidOnboard)
  x.AppNum.Load(row.AppNum)
  x.AppApi.Load(row.AppApi)
  x.SysApi.Load(row.SysApi)
  x.AppFwVersion.Load(row.AppFwVersion)
  x.SysFwVersion.Load(row.SysFwVersion)
  x.ComApi.Load(row.ComApi)
  x.ComFwVersion.Load(row.ComFwVersion)
  x.LastPktRcvd.Load(row.LastPktRcvd)
  x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
  x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
  x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
  x.LastResetCause.Load(row.LastResetCause)
  x.LastResetCauseTs.Load(row.LastResetCauseTs)
  x.LastRssi.Load(row.LastRssi)
  x.LastRssiTs.Load(row.LastRssiTs)
  x.LastSnr.Load(row.LastSnr)
  x.LastSnrTs.Load(row.LastSnrTs)
  x.NumDownstreams.Load(row.NumDownstreams)
  x.NumClearQueries.Load(row.NumClearQueries)
  x.AppLowPowerState.Load(row.AppLowPowerState)
  x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
  x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
  x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
  x.TSlot.Load(row.TSlot)
  x.TSlotValid.Load(row.TSlotValid)
  x.TSlotAssignedTs.Load(row.TSlotAssignedTs)
  x.ActualStateA.Load(row.ActualStateA)
  x.ActualStateB.Load(row.ActualStateB)
  x.ExpectedStateA.Load(row.ExpectedStateA)
  x.ExpectedStateB.Load(row.ExpectedStateB)
  return x.insert()
}

func (x *NodeState_Appnum4Appapi3Sysapi6) insert() error {
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  _, err := x.db.Exec(insertString,
   x.Eui.Get(),
   x.ValidOnboard.Get(),
   x.AppNum.Get(),
   x.AppApi.Get(),
   x.SysApi.Get(),
   x.AppFwVersion.Get(),
   x.SysFwVersion.Get(),
   x.ComApi.Get(),
   x.ComFwVersion.Get(),
   x.LastPktRcvd.Get(),
   x.LastPktRcvdTs.Get(),
   x.LastVoltageRcvd.Get(),
   x.LastVoltageRcvdTs.Get(),
   x.LastResetCause.Get(),
   x.LastResetCauseTs.Get(),
   x.LastRssi.Get(),
   x.LastRssiTs.Get(),
   x.LastSnr.Get(),
   x.LastSnrTs.Get(),
   x.NumDownstreams.Get(),
   x.NumClearQueries.Get(),
   x.AppLowPowerState.Get(),
   x.GlobalDownstreamEnable.Get(),
   x.SystemDownstreamEnable.Get(),
   x.AppDownstreamEnable.Get(),
   x.TSlot.Get(),
   x.TSlotValid.Get(),
   x.TSlotAssignedTs.Get(),
   x.ActualStateA.Get(),
   x.ActualStateB.Get(),
   x.ExpectedStateA.Get(),
   x.ExpectedStateB.Get(),
  )
  if err != nil {
    return err
  }
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *NodeState_Appnum4Appapi3Sysapi6) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  //fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    if x.txValid {
      _, err = x.tx.Exec(updateString)
    } else {
      _, err = x.db.Exec(updateString)
    }
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *NodeState_Appnum4Appapi3Sysapi6) SelectFromCurrent(eui string, lockRow bool) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  var err error
  var tx *sql.Tx
  x.txValid = lockRow
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  if x.txValid {
    tx, err = x.db.Begin()
    x.tx = tx
    if err != nil {
      //fmt.Printf("selectForCurrent x.db.Begin() failed\n")
      return err, false
    }
  }

  var results *sql.Rows
  if x.txValid {
    results, err = tx.Query(selectString)
  } else {
    results, err = x.db.Query(selectString)
  }
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return err, false
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.AppNum, &row.AppApi, &row.SysApi, &row.AppFwVersion, &row.SysFwVersion, &row.ComApi, &row.ComFwVersion, &row.LastPktRcvd, &row.LastPktRcvdTs, &row.LastVoltageRcvd, &row.LastVoltageRcvdTs, &row.LastResetCause, &row.LastResetCauseTs, &row.LastRssi, &row.LastRssiTs, &row.LastSnr, &row.LastSnrTs, &row.NumDownstreams, &row.NumClearQueries, &row.AppLowPowerState, &row.GlobalDownstreamEnable, &row.SystemDownstreamEnable, &row.AppDownstreamEnable, &row.TSlot, &row.TSlotValid, &row.TSlotAssignedTs, &row.ActualStateA, &row.ActualStateB, &row.ExpectedStateA, &row.ExpectedStateB,)
  if err != nil {
  return err, false
  }
 x.Eui.Load(row.Eui)
 x.ValidOnboard.Load(row.ValidOnboard)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.AppNum.Load(row.AppNum)
 x.AppApi.Load(row.AppApi)
 x.SysApi.Load(row.SysApi)
 x.AppFwVersion.Load(row.AppFwVersion)
 x.SysFwVersion.Load(row.SysFwVersion)
 x.ComApi.Load(row.ComApi)
 x.ComFwVersion.Load(row.ComFwVersion)
 x.LastPktRcvd.Load(row.LastPktRcvd)
 x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
 x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
 x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
 x.LastResetCause.Load(row.LastResetCause)
 x.LastResetCauseTs.Load(row.LastResetCauseTs)
 x.LastRssi.Load(row.LastRssi)
 x.LastRssiTs.Load(row.LastRssiTs)
 x.LastSnr.Load(row.LastSnr)
 x.LastSnrTs.Load(row.LastSnrTs)
 x.NumDownstreams.Load(row.NumDownstreams)
 x.NumClearQueries.Load(row.NumClearQueries)
 x.AppLowPowerState.Load(row.AppLowPowerState)
 x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
 x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
 x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
 x.TSlot.Load(row.TSlot)
 x.TSlotValid.Load(row.TSlotValid)
 x.TSlotAssignedTs.Load(row.TSlotAssignedTs)
 x.ActualStateA.Load(row.ActualStateA)
 x.ActualStateB.Load(row.ActualStateB)
 x.ExpectedStateA.Load(row.ExpectedStateA)
 x.ExpectedStateB.Load(row.ExpectedStateB)
    return nil, true
  }
  return nil, false
}

// DRT - This function currently DNE within guild. It would make sense to implement this and have SelectFromCurrent to call this func
//       For now I am not allowing this function to lock the row
func (x *NodeState_Appnum4Appapi3Sysapi6) GetRowByEui(eui string) (Row, error) {
  var row Row
  tableName := x.GetCurrentTableName()
  var err error
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  var results *sql.Rows
  results, err = x.db.Query(selectString)
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return row, err 
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.AppNum, &row.AppApi, &row.SysApi, &row.AppFwVersion, &row.SysFwVersion, &row.ComApi, &row.ComFwVersion, &row.LastPktRcvd, &row.LastPktRcvdTs, &row.LastVoltageRcvd, &row.LastVoltageRcvdTs, &row.LastResetCause, &row.LastResetCauseTs, &row.LastRssi, &row.LastRssiTs, &row.LastSnr, &row.LastSnrTs, &row.NumDownstreams, &row.NumClearQueries, &row.AppLowPowerState, &row.GlobalDownstreamEnable, &row.SystemDownstreamEnable, &row.AppDownstreamEnable, &row.TSlot, &row.TSlotValid, &row.TSlotAssignedTs, &row.ActualStateA, &row.ActualStateB, &row.ExpectedStateA, &row.ExpectedStateB,)
    return row, err
  }
  return row, err
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *NodeState_Appnum4Appapi3Sysapi6) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *NodeState_Appnum4Appapi3Sysapi6) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.AppNum, &row.AppApi, &row.SysApi, &row.AppFwVersion, &row.SysFwVersion, &row.ComApi, &row.ComFwVersion, &row.LastPktRcvd, &row.LastPktRcvdTs, &row.LastVoltageRcvd, &row.LastVoltageRcvdTs, &row.LastResetCause, &row.LastResetCauseTs, &row.LastRssi, &row.LastRssiTs, &row.LastSnr, &row.LastSnrTs, &row.NumDownstreams, &row.NumClearQueries, &row.AppLowPowerState, &row.GlobalDownstreamEnable, &row.SystemDownstreamEnable, &row.AppDownstreamEnable, &row.TSlot, &row.TSlotValid, &row.TSlotAssignedTs, &row.ActualStateA, &row.ActualStateB, &row.ExpectedStateA, &row.ExpectedStateB,)
    if err != nil {
       return err, false
    }
    x.Eui.Load(row.Eui)
    x.ValidOnboard.Load(row.ValidOnboard)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.AppNum.Load(row.AppNum)
    x.AppApi.Load(row.AppApi)
    x.SysApi.Load(row.SysApi)
    x.AppFwVersion.Load(row.AppFwVersion)
    x.SysFwVersion.Load(row.SysFwVersion)
    x.ComApi.Load(row.ComApi)
    x.ComFwVersion.Load(row.ComFwVersion)
    x.LastPktRcvd.Load(row.LastPktRcvd)
    x.LastPktRcvdTs.Load(row.LastPktRcvdTs)
    x.LastVoltageRcvd.Load(row.LastVoltageRcvd)
    x.LastVoltageRcvdTs.Load(row.LastVoltageRcvdTs)
    x.LastResetCause.Load(row.LastResetCause)
    x.LastResetCauseTs.Load(row.LastResetCauseTs)
    x.LastRssi.Load(row.LastRssi)
    x.LastRssiTs.Load(row.LastRssiTs)
    x.LastSnr.Load(row.LastSnr)
    x.LastSnrTs.Load(row.LastSnrTs)
    x.NumDownstreams.Load(row.NumDownstreams)
    x.NumClearQueries.Load(row.NumClearQueries)
    x.AppLowPowerState.Load(row.AppLowPowerState)
    x.GlobalDownstreamEnable.Load(row.GlobalDownstreamEnable)
    x.SystemDownstreamEnable.Load(row.SystemDownstreamEnable)
    x.AppDownstreamEnable.Load(row.AppDownstreamEnable)
    x.TSlot.Load(row.TSlot)
    x.TSlotValid.Load(row.TSlotValid)
    x.TSlotAssignedTs.Load(row.TSlotAssignedTs)
    x.ActualStateA.Load(row.ActualStateA)
    x.ActualStateB.Load(row.ActualStateB)
    x.ExpectedStateA.Load(row.ExpectedStateA)
    x.ExpectedStateB.Load(row.ExpectedStateB)
    return nil, true
  }
  return nil, false
}

func (x *NodeState_Appnum4Appapi3Sysapi6) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.ValidOnboard.String() + " " + x.ValidOnboard.Type() + " " + x.ValidOnboard.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.AppNum.String() + " " + x.AppNum.Type() + " " + x.AppNum.Modifiers() +
  ", " + x.AppApi.String() + " " + x.AppApi.Type() + " " + x.AppApi.Modifiers() +
  ", " + x.SysApi.String() + " " + x.SysApi.Type() + " " + x.SysApi.Modifiers() +
  ", " + x.AppFwVersion.String() + " " + x.AppFwVersion.Type() + " " + x.AppFwVersion.Modifiers() +
  ", " + x.SysFwVersion.String() + " " + x.SysFwVersion.Type() + " " + x.SysFwVersion.Modifiers() +
  ", " + x.ComApi.String() + " " + x.ComApi.Type() + " " + x.ComApi.Modifiers() +
  ", " + x.ComFwVersion.String() + " " + x.ComFwVersion.Type() + " " + x.ComFwVersion.Modifiers() +
  ", " + x.LastPktRcvd.String() + " " + x.LastPktRcvd.Type() + " " + x.LastPktRcvd.Modifiers() +
  ", " + x.LastPktRcvdTs.String() + " " + x.LastPktRcvdTs.Type() + " " + x.LastPktRcvdTs.Modifiers() +
  ", " + x.LastVoltageRcvd.String() + " " + x.LastVoltageRcvd.Type() + " " + x.LastVoltageRcvd.Modifiers() +
  ", " + x.LastVoltageRcvdTs.String() + " " + x.LastVoltageRcvdTs.Type() + " " + x.LastVoltageRcvdTs.Modifiers() +
  ", " + x.LastResetCause.String() + " " + x.LastResetCause.Type() + " " + x.LastResetCause.Modifiers() +
  ", " + x.LastResetCauseTs.String() + " " + x.LastResetCauseTs.Type() + " " + x.LastResetCauseTs.Modifiers() +
  ", " + x.LastRssi.String() + " " + x.LastRssi.Type() + " " + x.LastRssi.Modifiers() +
  ", " + x.LastRssiTs.String() + " " + x.LastRssiTs.Type() + " " + x.LastRssiTs.Modifiers() +
  ", " + x.LastSnr.String() + " " + x.LastSnr.Type() + " " + x.LastSnr.Modifiers() +
  ", " + x.LastSnrTs.String() + " " + x.LastSnrTs.Type() + " " + x.LastSnrTs.Modifiers() +
  ", " + x.NumDownstreams.String() + " " + x.NumDownstreams.Type() + " " + x.NumDownstreams.Modifiers() +
  ", " + x.NumClearQueries.String() + " " + x.NumClearQueries.Type() + " " + x.NumClearQueries.Modifiers() +
  ", " + x.AppLowPowerState.String() + " " + x.AppLowPowerState.Type() + " " + x.AppLowPowerState.Modifiers() +
  ", " + x.GlobalDownstreamEnable.String() + " " + x.GlobalDownstreamEnable.Type() + " " + x.GlobalDownstreamEnable.Modifiers() +
  ", " + x.SystemDownstreamEnable.String() + " " + x.SystemDownstreamEnable.Type() + " " + x.SystemDownstreamEnable.Modifiers() +
  ", " + x.AppDownstreamEnable.String() + " " + x.AppDownstreamEnable.Type() + " " + x.AppDownstreamEnable.Modifiers() +
  ", " + x.TSlot.String() + " " + x.TSlot.Type() + " " + x.TSlot.Modifiers() +
  ", " + x.TSlotValid.String() + " " + x.TSlotValid.Type() + " " + x.TSlotValid.Modifiers() +
  ", " + x.TSlotAssignedTs.String() + " " + x.TSlotAssignedTs.Type() + " " + x.TSlotAssignedTs.Modifiers() +
  ", " + x.ActualStateA.String() + " " + x.ActualStateA.Type() + " " + x.ActualStateA.Modifiers() +
  ", " + x.ActualStateB.String() + " " + x.ActualStateB.Type() + " " + x.ActualStateB.Modifiers() +
  ", " + x.ExpectedStateA.String() + " " + x.ExpectedStateA.Type() + " " + x.ExpectedStateA.Modifiers() +
  ", " + x.ExpectedStateB.String() + " " + x.ExpectedStateB.Type() + " " + x.ExpectedStateB.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *NodeState_Appnum4Appapi3Sysapi6) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *NodeState_Appnum4Appapi3Sysapi6) SelectString(tableName string) string {
  if x.txValid {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + " for update;"
  } else {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
  }
}

// insert is only done to the current table
func (x *NodeState_Appnum4Appapi3Sysapi6) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.ValidOnboard.String() + " , " +  x.AppNum.String() + " , " +  x.AppApi.String() + " , " +  x.SysApi.String() + " , " +  x.AppFwVersion.String() + " , " +  x.SysFwVersion.String() + " , " +  x.ComApi.String() + " , " +  x.ComFwVersion.String() + " , " +  x.LastPktRcvd.String() + " , " +  x.LastPktRcvdTs.String() + " , " +  x.LastVoltageRcvd.String() + " , " +  x.LastVoltageRcvdTs.String() + " , " +  x.LastResetCause.String() + " , " +  x.LastResetCauseTs.String() + " , " +  x.LastRssi.String() + " , " +  x.LastRssiTs.String() + " , " +  x.LastSnr.String() + " , " +  x.LastSnrTs.String() + " , " +  x.NumDownstreams.String() + " , " +  x.NumClearQueries.String() + " , " +  x.AppLowPowerState.String() + " , " +  x.GlobalDownstreamEnable.String() + " , " +  x.SystemDownstreamEnable.String() + " , " +  x.AppDownstreamEnable.String() + " , " +  x.TSlot.String() + " , " +  x.TSlotValid.String() + " , " +  x.TSlotAssignedTs.String() + " , " +  x.ActualStateA.String() + " , " +  x.ActualStateB.String() + " , " +  x.ExpectedStateA.String() + " , " +  x.ExpectedStateB.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *NodeState_Appnum4Appapi3Sysapi6) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.ValidOnboard.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValidOnboard.String() + " = " + x.ValidOnboard.QueryValue()
    first = false
  }
  if x.AppNum.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppNum.String() + " = " + x.AppNum.QueryValue()
    first = false
  }
  if x.AppApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppApi.String() + " = " + x.AppApi.QueryValue()
    first = false
  }
  if x.SysApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SysApi.String() + " = " + x.SysApi.QueryValue()
    first = false
  }
  if x.AppFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppFwVersion.String() + " = " + x.AppFwVersion.QueryValue()
    first = false
  }
  if x.SysFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SysFwVersion.String() + " = " + x.SysFwVersion.QueryValue()
    first = false
  }
  if x.ComApi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ComApi.String() + " = " + x.ComApi.QueryValue()
    first = false
  }
  if x.ComFwVersion.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ComFwVersion.String() + " = " + x.ComFwVersion.QueryValue()
    first = false
  }
  if x.LastPktRcvd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktRcvd.String() + " = " + x.LastPktRcvd.QueryValue()
    first = false
  }
  if x.LastPktRcvdTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktRcvdTs.String() + " = " + x.LastPktRcvdTs.QueryValue()
    first = false
  }
  if x.LastVoltageRcvd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastVoltageRcvd.String() + " = " + x.LastVoltageRcvd.QueryValue()
    first = false
  }
  if x.LastVoltageRcvdTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastVoltageRcvdTs.String() + " = " + x.LastVoltageRcvdTs.QueryValue()
    first = false
  }
  if x.LastResetCause.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastResetCause.String() + " = " + x.LastResetCause.QueryValue()
    first = false
  }
  if x.LastResetCauseTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastResetCauseTs.String() + " = " + x.LastResetCauseTs.QueryValue()
    first = false
  }
  if x.LastRssi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastRssi.String() + " = " + x.LastRssi.QueryValue()
    first = false
  }
  if x.LastRssiTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastRssiTs.String() + " = " + x.LastRssiTs.QueryValue()
    first = false
  }
  if x.LastSnr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastSnr.String() + " = " + x.LastSnr.QueryValue()
    first = false
  }
  if x.LastSnrTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastSnrTs.String() + " = " + x.LastSnrTs.QueryValue()
    first = false
  }
  if x.NumDownstreams.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.NumDownstreams.String() + " = " + x.NumDownstreams.QueryValue()
    first = false
  }
  if x.NumClearQueries.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.NumClearQueries.String() + " = " + x.NumClearQueries.QueryValue()
    first = false
  }
  if x.AppLowPowerState.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppLowPowerState.String() + " = " + x.AppLowPowerState.QueryValue()
    first = false
  }
  if x.GlobalDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.GlobalDownstreamEnable.String() + " = " + x.GlobalDownstreamEnable.QueryValue()
    first = false
  }
  if x.SystemDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SystemDownstreamEnable.String() + " = " + x.SystemDownstreamEnable.QueryValue()
    first = false
  }
  if x.AppDownstreamEnable.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AppDownstreamEnable.String() + " = " + x.AppDownstreamEnable.QueryValue()
    first = false
  }
  if x.TSlot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TSlot.String() + " = " + x.TSlot.QueryValue()
    first = false
  }
  if x.TSlotValid.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TSlotValid.String() + " = " + x.TSlotValid.QueryValue()
    first = false
  }
  if x.TSlotAssignedTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TSlotAssignedTs.String() + " = " + x.TSlotAssignedTs.QueryValue()
    first = false
  }
  if x.ActualStateA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ActualStateA.String() + " = " + x.ActualStateA.QueryValue()
    first = false
  }
  if x.ActualStateB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ActualStateB.String() + " = " + x.ActualStateB.QueryValue()
    first = false
  }
  if x.ExpectedStateA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ExpectedStateA.String() + " = " + x.ExpectedStateA.QueryValue()
    first = false
  }
  if x.ExpectedStateB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ExpectedStateB.String() + " = " + x.ExpectedStateB.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *NodeState_Appnum4Appapi3Sysapi6) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

