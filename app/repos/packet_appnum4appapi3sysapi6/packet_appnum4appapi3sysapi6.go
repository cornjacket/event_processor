package packet_appnum4appapi3sysapi6

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/id"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/eui"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/createdat"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/updatedat"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/ts"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/dr"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/ack"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/cmd"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/snr"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/data"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/fcnt"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/freq"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/port"
  "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6/main/rssi"
)

type Row struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  Ts	int64	`json:"ts"`
  Dr	string	`json:"dr"`
  Ack	int	`json:"ack"`
  Cmd	string	`json:"cmd"`
  Snr	int	`json:"snr"`
  Data	string	`json:"data"`
  Fcnt	int	`json:"fcnt"`
  Freq	int64	`json:"freq"`
  Port	int	`json:"port"`
  Rssi	int	`json:"rssi"`
}

type PrevRow struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  Ts	int64	`json:"ts"`
  Dr	string	`json:"dr"`
  Ack	int	`json:"ack"`
  Cmd	string	`json:"cmd"`
  Snr	int	`json:"snr"`
  Data	string	`json:"data"`
  Fcnt	int	`json:"fcnt"`
  Freq	int64	`json:"freq"`
  Port	int	`json:"port"`
  Rssi	int	`json:"rssi"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("Packet_AppNum4AppApi3SysApi6.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("Packet_AppNum4AppApi3SysApi6.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  packet_appnum4appapi3sysapi6 := New()
  //currentTable := packet_appnum4appapi3sysapi6.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, packet_appnum4appapi3sysapi6.CreateTableString())
  // Create table if it doesn't already exist
  err := packet_appnum4appapi3sysapi6.CreateTable()
  if err != nil {
    fmt.Printf("packet_appnum4appapi3sysapi6.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("packet_appnum4appapi3sysapi6.CreateTable() succeeded!!\n")
  }
}

type Packet_AppNum4AppApi3SysApi6 struct {
  db           *sql.DB
  tx           *sql.Tx // used for the transaction
  txValid      bool // determines whether a transaction is currently in progress
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Id	*id.Id
  Eui	*eui.Eui
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  Ts	*ts.Ts
  Dr	*dr.Dr
  Ack	*ack.Ack
  Cmd	*cmd.Cmd
  Snr	*snr.Snr
  Data	*data.Data
  Fcnt	*fcnt.Fcnt
  Freq	*freq.Freq
  Port	*port.Port
  Rssi	*rssi.Rssi
}

func New() *Packet_AppNum4AppApi3SysApi6 {
  x := &Packet_AppNum4AppApi3SysApi6{}
  x.db = db_conn
  x.txValid = false
  x.tableName = "packet_appnum4appapi3sysapi6"
  x.prevVersion = 1
  x.version = 2
  x.Id=	id.New()
  x.Eui=	eui.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.Ts=	ts.New()
  x.Dr=	dr.New()
  x.Ack=	ack.New()
  x.Cmd=	cmd.New()
  x.Snr=	snr.New()
  x.Data=	data.New()
  x.Fcnt=	fcnt.New()
  x.Freq=	freq.New()
  x.Port=	port.New()
  x.Rssi=	rssi.New()
  x.constraints = "constraint pk_example primary key (id)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

// Close() should be called after all DB operations are done. If a transaction is currently in progress, then Close will Rollback or Commit based on the DB error
// sent to Close(). If Close or Rollback has already been called by the user, then calling Close() should have no effect.
func (x *Packet_AppNum4AppApi3SysApi6) Close (err error) (error, bool) {
  if x.txValid {
    if err != nil {
      return x.tx.Rollback(), true
    } else {
      return x.tx.Commit(), true
    }
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call rollback, releasing the lock
func (x *Packet_AppNum4AppApi3SysApi6) Rollback () (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Rollback(), true
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call commit, releasing the lock
func (x *Packet_AppNum4AppApi3SysApi6) Commit() (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Commit(), true
  }
  return nil, false
}

func (x *Packet_AppNum4AppApi3SysApi6) GetByEui (eui string, createIfNotFound bool, lockRow bool) (error, bool) {
  //fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui, lockRow)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    //fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  //fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      //fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        //fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        //fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
    // TODO: If lockRow, then SelectFromCurrent (with lock on) should be called again since now the row has been created... otherwise there is a potential race condition
  }
  return err, rcvd
}

func (x *Packet_AppNum4AppApi3SysApi6) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *Packet_AppNum4AppApi3SysApi6) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *Packet_AppNum4AppApi3SysApi6) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *Packet_AppNum4AppApi3SysApi6) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.Ts.Load(row.Ts)
  x.Dr.Load(row.Dr)
  x.Ack.Load(row.Ack)
  x.Cmd.Load(row.Cmd)
  x.Snr.Load(row.Snr)
  x.Data.Load(row.Data)
  x.Fcnt.Load(row.Fcnt)
  x.Freq.Load(row.Freq)
  x.Port.Load(row.Port)
  x.Rssi.Load(row.Rssi)
  return x.insert()
}

func (x *Packet_AppNum4AppApi3SysApi6) insert() error {
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  // insert is now not need with Eui
  insert, err := x.db.Exec(insertString,
   x.Eui.Get(),
   x.Ts.Get(),
   x.Dr.Get(),
   x.Ack.Get(),
   x.Cmd.Get(),
   x.Snr.Get(),
   x.Data.Get(),
   x.Fcnt.Get(),
   x.Freq.Get(),
   x.Port.Get(),
   x.Rssi.Get(),
  )
  if err != nil {
    return err
  }
  Id, err := insert.LastInsertId() // Id is not defined in EUI context, though it could be useful in other contexts
  if err != nil {
    return err
  }
  x.Id.Load(Id)
  //fmt.Printf("Insert id: %d\n", x.Id)
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Packet_AppNum4AppApi3SysApi6) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  //fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    if x.txValid {
      _, err = x.tx.Exec(updateString)
    } else {
      _, err = x.db.Exec(updateString)
    }
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *Packet_AppNum4AppApi3SysApi6) SelectFromCurrent(eui string, lockRow bool) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  var err error
  var tx *sql.Tx
  x.txValid = lockRow
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  if x.txValid {
    tx, err = x.db.Begin()
    x.tx = tx
    if err != nil {
      //fmt.Printf("selectForCurrent x.db.Begin() failed\n")
      return err, false
    }
  }

  var results *sql.Rows
  if x.txValid {
    results, err = tx.Query(selectString)
  } else {
    results, err = x.db.Query(selectString)
  }
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return err, false
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.Dr, &row.Ack, &row.Cmd, &row.Snr, &row.Data, &row.Fcnt, &row.Freq, &row.Port, &row.Rssi,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.Ts.Load(row.Ts)
 x.Dr.Load(row.Dr)
 x.Ack.Load(row.Ack)
 x.Cmd.Load(row.Cmd)
 x.Snr.Load(row.Snr)
 x.Data.Load(row.Data)
 x.Fcnt.Load(row.Fcnt)
 x.Freq.Load(row.Freq)
 x.Port.Load(row.Port)
 x.Rssi.Load(row.Rssi)
    return nil, true
  }
  return nil, false
}

func (x *Packet_AppNum4AppApi3SysApi6) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *Packet_AppNum4AppApi3SysApi6) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *Packet_AppNum4AppApi3SysApi6) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.Dr, &row.Ack, &row.Cmd, &row.Snr, &row.Data, &row.Fcnt, &row.Freq, &row.Port, &row.Rssi,)
    if err != nil {
       return err, false
    }
    x.Id.Load(row.Id)
    x.Eui.Load(row.Eui)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.Ts.Load(row.Ts)
    x.Dr.Load(row.Dr)
    x.Ack.Load(row.Ack)
    x.Cmd.Load(row.Cmd)
    x.Snr.Load(row.Snr)
    x.Data.Load(row.Data)
    x.Fcnt.Load(row.Fcnt)
    x.Freq.Load(row.Freq)
    x.Port.Load(row.Port)
    x.Rssi.Load(row.Rssi)
    return nil, true
  }
  return nil, false
}

func (x *Packet_AppNum4AppApi3SysApi6) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Id.String() + " " + x.Id.Type() + " " + x.Id.Modifiers() +
  ", " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.Ts.String() + " " + x.Ts.Type() + " " + x.Ts.Modifiers() +
  ", " + x.Dr.String() + " " + x.Dr.Type() + " " + x.Dr.Modifiers() +
  ", " + x.Ack.String() + " " + x.Ack.Type() + " " + x.Ack.Modifiers() +
  ", " + x.Cmd.String() + " " + x.Cmd.Type() + " " + x.Cmd.Modifiers() +
  ", " + x.Snr.String() + " " + x.Snr.Type() + " " + x.Snr.Modifiers() +
  ", " + x.Data.String() + " " + x.Data.Type() + " " + x.Data.Modifiers() +
  ", " + x.Fcnt.String() + " " + x.Fcnt.Type() + " " + x.Fcnt.Modifiers() +
  ", " + x.Freq.String() + " " + x.Freq.Type() + " " + x.Freq.Modifiers() +
  ", " + x.Port.String() + " " + x.Port.Type() + " " + x.Port.Modifiers() +
  ", " + x.Rssi.String() + " " + x.Rssi.Type() + " " + x.Rssi.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *Packet_AppNum4AppApi3SysApi6) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *Packet_AppNum4AppApi3SysApi6) SelectString(tableName string) string {
  if x.txValid {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + " for update;"
  } else {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
  }
}

// insert is only done to the current table
func (x *Packet_AppNum4AppApi3SysApi6) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.Ts.String() + " , " +  x.Dr.String() + " , " +  x.Ack.String() + " , " +  x.Cmd.String() + " , " +  x.Snr.String() + " , " +  x.Data.String() + " , " +  x.Fcnt.String() + " , " +  x.Freq.String() + " , " +  x.Port.String() + " , " +  x.Rssi.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *Packet_AppNum4AppApi3SysApi6) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.Ts.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ts.String() + " = " + x.Ts.QueryValue()
    first = false
  }
  if x.Dr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Dr.String() + " = " + x.Dr.QueryValue()
    first = false
  }
  if x.Ack.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ack.String() + " = " + x.Ack.QueryValue()
    first = false
  }
  if x.Cmd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Cmd.String() + " = " + x.Cmd.QueryValue()
    first = false
  }
  if x.Snr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Snr.String() + " = " + x.Snr.QueryValue()
    first = false
  }
  if x.Data.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Data.String() + " = " + x.Data.QueryValue()
    first = false
  }
  if x.Fcnt.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Fcnt.String() + " = " + x.Fcnt.QueryValue()
    first = false
  }
  if x.Freq.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Freq.String() + " = " + x.Freq.QueryValue()
    first = false
  }
  if x.Port.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Port.String() + " = " + x.Port.QueryValue()
    first = false
  }
  if x.Rssi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Rssi.String() + " = " + x.Rssi.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *Packet_AppNum4AppApi3SysApi6) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

func (x *Packet_AppNum4AppApi3SysApi6) DeleteByIdString(tableName string) string {
  return "delete from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}

// function returns true if at least one field has been updated
func (x *Packet_AppNum4AppApi3SysApi6) UpdateByIdString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.Ts.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ts.String() + " = " + x.Ts.QueryValue()
    first = false
  }
  if x.Dr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Dr.String() + " = " + x.Dr.QueryValue()
    first = false
  }
  if x.Ack.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ack.String() + " = " + x.Ack.QueryValue()
    first = false
  }
  if x.Cmd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Cmd.String() + " = " + x.Cmd.QueryValue()
    first = false
  }
  if x.Snr.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Snr.String() + " = " + x.Snr.QueryValue()
    first = false
  }
  if x.Data.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Data.String() + " = " + x.Data.QueryValue()
    first = false
  }
  if x.Fcnt.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Fcnt.String() + " = " + x.Fcnt.QueryValue()
    first = false
  }
  if x.Freq.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Freq.String() + " = " + x.Freq.QueryValue()
    first = false
  }
  if x.Port.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Port.String() + " = " + x.Port.QueryValue()
    first = false
  }
  if x.Rssi.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Rssi.String() + " = " + x.Rssi.QueryValue()
    first = false
  }
  return_str += " where id = " + x.Id.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Packet_AppNum4AppApi3SysApi6) UpdateById() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateByIdString()
  if anyFieldModified {
    //fmt.Printf("table.UpdateByIdString(): %s\n", updateString)

    _, err := x.db.Exec(updateString,
    )
    if err != nil {
      return anyFieldModified, err
    }
  } else {
    fmt.Printf("table.UpdateById: No columns modified\n")
  }
  return anyFieldModified, nil
}

func (x *Packet_AppNum4AppApi3SysApi6) SelectByIdFromCurrent(id int64) (error, bool) {  //d.Id = id
  x.Id.Load(id)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectByIdString(tableName)
  //fmt.Printf("table.SelectByIdString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.Dr, &row.Ack, &row.Cmd, &row.Snr, &row.Data, &row.Fcnt, &row.Freq, &row.Port, &row.Rssi,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.Ts.Load(row.Ts)
 x.Dr.Load(row.Dr)
 x.Ack.Load(row.Ack)
 x.Cmd.Load(row.Cmd)
 x.Snr.Load(row.Snr)
 x.Data.Load(row.Data)
 x.Fcnt.Load(row.Fcnt)
 x.Freq.Load(row.Freq)
 x.Port.Load(row.Port)
 x.Rssi.Load(row.Rssi)
    return nil, true
  }
  return nil, false
}

func (x *Packet_AppNum4AppApi3SysApi6) SelectByIdString(tableName string) string {
  return "select * from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}

