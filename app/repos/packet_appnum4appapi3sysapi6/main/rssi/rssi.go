package rssi

import (
  "strconv"
)

type Rssi struct {
  value int
  updated bool
}

func New() *Rssi {
  return &Rssi{}
}

func (x *Rssi) IsUpdated() bool {
  return x.updated
}

func (x *Rssi) Get() int {
  return x.value
}

func (x *Rssi) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Rssi) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Rssi) String() string {
  return "Rssi"
}

func (x *Rssi) Type() string {
  return "int"
}

func (x *Rssi) Modifiers() string {
  return "not null"
}

func (x *Rssi) QueryValue() string {
  return strconv.Itoa(x.value)
}

