package controllers

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/cornjacket/event_processor/app/utils/env"
	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

func (app *AppContext) initPacketIngressFromEnv() {
	brokerAddr := os.Getenv("EVENT_HNDLR_RABBITMQ_BROKER_ADDR")
	kafkaHostname := os.Getenv("EVENT_HNDLR_KAFKA_HOSTNAME")
	if len(brokerAddr) != 0 {
		app.PacketIngressTransportType = RabbitMQ
		app.BrokerAddress = brokerAddr
	} else if len(kafkaHostname) != 0 {
		app.PacketIngressTransportType = Kafka
		kafkaPort := env.GetVar("EVENT_HNDLR_KAFKA_PORT", "9092", false)
		app.KafkaURL = kafkaHostname + ":" + kafkaPort
	} else { // default
		app.PacketIngressTransportType = HttpPost
	}
}

func (app *AppContext) initializeIngress() {

	fmt.Println("init Ingress ... !!")
	app.initPacketIngressFromEnv()

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))

	// Consul Heartbeat
	app.Router.GET("/info", wrapHandlerFunc(app.Info))

	// Packet Ingress
	switch app.PacketIngressTransportType {
	case RabbitMQ:
		go app.createRabbitMQWorkerPool()
	case Kafka:
		app.createKafkaWorkerPool()
	case HttpPost:
		app.Router.POST("/packet", app.NewParserWorkerPoolFunc(100))
	default:
		log.Fatal("Error in ingress.go. PacketIngressTransportType is not intialized.")
	}

	// TallocResp Router
	app.Router.POST("/tallocresp", app.NewTallocRespPoolEntryFunc(100))

	// Swagger API /docs path
	opts := middleware.RedocOpts{SpecURL: "/swagger.yaml"}
	sh := middleware.Redoc(opts, nil)
	app.Router.GET("/docs", wrapHandlerFunc(sh.ServeHTTP))
	app.Router.GET("/swagger.yaml", wrapHandlerFunc(http.FileServer(http.Dir("./app")).ServeHTTP))

	// TODO:
	// Add /ping
	// Add /health
	// Add version, lastboot, ... via status. See code in temp

}

// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}
