// Package classification Event Handler API.
//
// Documentation for Event Handler API
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http
//     Host: localhost
//     BasePath: /
//     Version: 0.1.1
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: David Taylor<taylor.david.ray@gmail.com> http://john.doe.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/cornjacket/iot/message"

	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client_lib"
	view_client "bitbucket.org/cornjacket/view_hndlr/client_lib"
	view_models "bitbucket.org/cornjacket/view_hndlr/app/models"
	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
)

type ViewHandlrPostInterface interface {
	Open() error
	SendStatus(data view_models.Data) error
	SendNodestate(nodestate *view_models.NodeState) error
	SendPacket(packet message.UpPacket) error
}

type AirTrafficControlPostInterface interface {
	//Open(Hostname, Port, PacketPath, TallocReqPath string, RetryNum int) error
	Open() error
	RequestTslot(tallocreq *atc_client.TAllocReq) error
}

// TODO: change guild so that it doesn't use package level scope. Instead create a type which lives in the AppContext
type AppContext struct {
	Router				*httprouter.Router
	ViewHndlrClient			ViewHandlrPostInterface
	AirTrafficControlService	AirTrafficControlPostInterface
	PacketIngressTransportType	IngressTransportType
	TallocRespIngressTransportType	IngressTransportType
	KafkaURL			string
	BrokerAddress			string
}

type IngressTransportType int

const (
	Disabled IngressTransportType = 0 // default
	HttpPost IngressTransportType = 1
	Kafka    IngressTransportType = 2
	RabbitMQ IngressTransportType = 3
)

func NewAppContext() *AppContext {
	context := AppContext{}
	context.ViewHndlrClient = &view_client.ViewHndlrClient{}                // implements ViewHandlrPostInterface
	context.AirTrafficControlService = &atc_client.AirTrafficControlClient{} // implements AirTrafficControlPostInterface
	context.TallocRespIngressTransportType = HttpPost
	return &context
}

func (app *AppContext) Run(addr string) {
	// Instantiate a new router
	app.Router = httprouter.New()

	// change this to init ingress
	app.initializeIngress()

	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}
