package controllers

import (
	// Standard library packages
	//"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client_lib"
	nodestate "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6"
	packet "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6"
	"bitbucket.org/cornjacket/event_processor/app/utils/parse"
	"bitbucket.org/cornjacket/iot/loriot"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	view_models "bitbucket.org/cornjacket/view_hndlr/app/models"
	"github.com/julienschmidt/httprouter"
)

// A message.UpPacket is returned in the response
// swagger:response packetResponse
type packetResponseWrapper struct {
	// Packet send to Event Handler
	// in: body
	Body message.UpPacket
}

// swagger:route POST /packet packet sendPacket
// Sends a LoRa Rx packet to the Event Handler for internal processing by the node state machine.
// The Event Handler will forward the packet to the View Handler for storage.
// Returns LoRa packet to client
// responses:
//   201: packetResponse

// This function returns the entry point for the packet processing worker pool upon reception of
// a post to /packet. The default behavior is for the pktproc2 post worker to immediately queue the packet
// followed by replying to the client with the posted packet.
func (app *AppContext) NewParserWorkerPoolFunc(numWorkers int) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return pktproc2.NewHttpPacketHandlerFunc(numWorkers, app.packetHandler)
}

// Shoule be PacketForwarder or PacketRouter

// This func returns true in all cases. Not so nice.
// TODO: Instead of returning bool, func should return err so caller can react
// TODO: On error upper layer logic should perform a backoff or alternate action instead of continually sending to a down service
// General non-optimal behavior.
// Endpoint controller can assume the appNum, appApi, and sysApi since the Data Stream triplet uniquely specifies this.
// 1. Receive packet - decoding happens in pktproc2 prior to calling user packet handler
// 2. Send packet to ViewHndlrService, on failure Ingest packet into packet table
// 3. Fetch node state based on packet's EUI
// 4. Parse packet's data field. Is it app/system/unknown?
// 5. Save next state including last received values
// 6. Downstream response to node
// 7. Send alert/notifications
// 8. Ingest data into view store

// The system controller should not respond to the node with application commands if the node has been moved to a different location. This can
// be identified via the gateway EUI that is received by airtrafficcontrol. This is a higher level concern...
func (app *AppContext) packetHandler(wID int, p message.UpPacket) bool {

	//loraPktSlice, validPacket := validatePacket(p)
	_, validPacket := validatePacket(p)
	if validPacket {
		//fmt.Printf("PktHandler: worker %d recieved Rx packet\n", wID)

		err := app.ViewHndlrClient.SendPacket(p)
		pktproc2.DisplayPassFailMessageWithLatency("Sent packet to ViewHandlr to log packet", err, wID, &p, true)
		// 2. Ingest packet into packet table if unable to send packet to ViewHandlr
		if err != nil {
			err = insertPktTable(&p)
			pktproc2.DisplayPassFailMessageWithLatency("Ingest to packet table due to ViewHandlr down", err, wID, &p, true)
		}

		err = app.fetchNodeStateAndTakeAction(wID, p)
		pktproc2.DisplayPassFailMessageWithLatency("Fetch NodeState and send to service", err, wID, &p, true)

		// 8. Ingest data into view store
		// TODO: The result of the nodeState lookup should result in an intrinsic id that would be used to associate with the data
		// TODO: The following should become a function and then turned into a go routine that runs in parallel
		isStatusPkt, stateA, stateB, _ := parseAppPacket(p.Data) // THIS IS REPLICATED CODE. Check fetchNodeState. I NEED TO REDESIGN HOW THE DATA MOVES.
		if isStatusPkt {
			fmt.Printf("pkt isStatusPkt: %v, stateA: %v, stateB: %v\n", isStatusPkt, stateA, stateB)
			data := newData(&p, stateA, stateB)
			err = app.ViewHndlrClient.SendStatus(data)
			pktproc2.DisplayPassFailMessageWithLatency("Send data to ViewHandlr", err, wID, &p, true)
		}

		// 6. Downstream response to node if needed
		// 7. Send alert/notifications

	} else {
		fmt.Printf("FAIL, classId: %s, corrId: %d, worker %d Unable to process non-Rx packet\n", p.ClassId, p.CorrId, wID) // An error should be generated. This path shouldn't happen
	}

	return true // this should be returning the error
}

func insertPktTable(p *message.UpPacket) error {
	pkt := packet.New()
	pktRow := packet2pktRow(p)
	err := pkt.Insert(&pktRow)
	return err
}

// TODO: This code should be redesigned using an FSM approach which will make it logically more simple.

// TODO: The are a few places where this function returns, it may be useful to return a code that indicates where the app terminated on error...
func (app *AppContext) fetchNodeStateAndTakeAction(wID int, p message.UpPacket) error {

	// Some of the code below can be re-written to take advantage of the fact that the packet has been converted into a slice
	loraPktSlice, _ := parse.HexStringToSlice(p.Data)
	opCode, _ := parse.Opcode(&loraPktSlice)

	// 3. Fetch node state based on packet's EUI, if not found, then we will create the Node state row
	nodeState := nodestate.New()
	var nodeRow nodestate.Row

	nodeRow.Eui = p.Eui

	var found bool
	createRowIfNotFound := true
	lockRow := false                                                            // ATC code correctly locks and unlocks. Review that code and revisit this.
	err, found := nodeState.GetByEui(nodeRow.Eui, createRowIfNotFound, lockRow) // TODO: The order of these return values are backwards. The generator needs to be fixed...
	if err != nil {
		fmt.Printf("FAIL, classId: %s, corrId: %d, worker %d GetByEui: err: %s\n", p.ClassId, p.CorrId, wID, err)
		return err
	}

	// check for not found. Then what should be done.
	// TODO: If packet is a cloudsync packet, then create a new row with valid_onboard.
	// Otherwise create a record and make valid_onboard = false, and send an alert.
	if found == true {
		markInvalid := false
		packet2nodeState(&p, nodeState, markInvalid)

		// 4. Parse packet's data field. Is it app/system/unknown?
		// valid indicates if an invalid packet was received, maybe that check should happen elsewhere
		isStatusPkt, stateA, stateB, _ := parseAppPacket(p.Data) // valid is not needed since the packet has already been check before entry
		//fmt.Printf("pkt isStatusPkt: %v, stateA: %v, stateB: %v, valid: %v\n", isStatusPkt, stateA, stateB, valid)

		// TODO: It could be a bunch of different packets. Right now we are assuming only cloudsync
		// and status, but there are others. And we should handle it by abstracting the system level
		// functions.
		if isCloudSyncPkt(opCode) {

			cloudSync2nodeState(&p, nodeState)
			if validTslot(nodeState) {
				cloudSyncResponse(nodeState)
			} else {
				tallocreq := atc_client.TAllocReq{NodeEui: p.Eui, AppNum: 4, AppApi: 3, SysApi: 6, TimeReq: 15, CorrId: p.CorrId, ClassId: p.ClassId, Hops: p.Hops, Ts: p.Ts}
				err := app.AirTrafficControlService.RequestTslot(&tallocreq)
				pktproc2.DisplayPassFailMessageWithLatency("Send TslotReq to ATC", err, wID, &p, false)
				//if err != nil {
				//	fmt.Printf("FAIL, classId: %s, corrId: %d, worker %d Error sending TslotReq to ATC: err: %s\n", p.ClassId, p.CorrId, wID, err)
				//}
			}

		} else if isStatusPkt {
			// TODO: if isStatusPkt, we should add in StatusA and StatusB into this update.  Add StatusRcvdTs.
			nodeState.ActualStateA.Set(int(stateA))
			nodeState.ActualStateB.Set(int(stateB))
			// TODO: Need to add TsRcvd Column to the nodeState table
		}

	} else {
		// not found
		// if the node has not cloud sync'd then the application controller can not interpret any upstream data
		if isCloudSyncPkt(opCode) {
			cloudSync2nodeState(&p, nodeState)
			tallocreq := atc_client.TAllocReq{NodeEui: p.Eui, AppNum: 4, AppApi: 3, SysApi: 6, TimeReq: 15, CorrId: p.CorrId, ClassId: p.ClassId, Hops: p.Hops, Ts: p.Ts}
			app.AirTrafficControlService.RequestTslot(&tallocreq)
		} else {
			// if not found in node state table, then for now onboard as invalid
			// First let's handle non-cloud sync packet, later handle cloudsync packet
			markInvalid := true
			packet2nodeState(&p, nodeState, markInvalid)
		}

	}

	// 5. Save node state including last received values
	_, err = nodeState.Update()
	pktproc2.DisplayPassFailMessageWithLatency("Update NodeState", err, wID, &p, true)

	// Should I cast GetRow() to a type that belongs to the ViewHandler client? DRT
	nodeStateRow := view_models.NodeState(nodeState.GetRow())
	err = app.ViewHndlrClient.SendNodestate(&nodeStateRow)

	return err

}

func cloudSyncResponse(nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6) {

	fmt.Printf("cloudSyncResponse. TODO: send response to node EUI: %s with TSlot: %d\n", nodeState.Eui.Get(), nodeState.TSlot.Get())

}

func validTslot(nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6) bool {
	return nodeState.TSlotValid.Get() == 1
}

func newData(p *message.UpPacket, stateA byte, stateB byte) view_models.Data {
	data := view_models.Data{}
	data.Ts = p.Ts
	data.ValueA = int(stateA)
	data.ValueB = int(stateB)
	data.Eui = p.Eui
	data.ClassId = p.ClassId
	data.CorrId = p.CorrId
	data.Hops = p.Hops + 1
	return data
}

func validatePacket(p message.UpPacket) ([]byte, bool) {
	if loriot.IsRxPacket(p) == false {
		return nil, false
	}
	return parse.HexStringToSlice(p.Data)
}

// This is a standard function converting from loriot packet to packet SQL row datatype. For now this func lives in the packet controller but
// in the future the loriot packet table could become a standard structure allowing this function to become common.
func packet2pktRow(p *message.UpPacket) packet.Row {
	pktRow := packet.Row{}
	pktRow.Ts = int64(p.Ts)
	pktRow.Dr = p.Dr
	pktRow.Eui = p.Eui
	if p.Ack {
		pktRow.Ack = 1
	}
	pktRow.Cmd = p.Cmd
	pktRow.Snr = int(p.Snr * 10)
	pktRow.Data = p.Data
	pktRow.Fcnt = p.Fcnt
	pktRow.Freq = int64(p.Freq) // TODO: Is p.Freq too small to hold the freq?
	pktRow.Port = p.Port
	pktRow.Rssi = p.Rssi
	return pktRow
}

// TODO: It may be useful to have this function be part of an application specific library, removing it from this file.
func parseAppPacket(data string) (bool, byte, byte, bool) {
	var opCode byte
	var status byte
	var isStatusPkt bool
	loraPktSlice, ok := parse.HexStringToSlice(data)
	if ok {
		opCode, ok = parse.Opcode(&loraPktSlice)
		if ok {
			isStatusPkt = isStatusPacket(opCode)
			if isStatusPkt {
				status, ok = parse.StatusByte(&loraPktSlice)
			}
		}
	}
	// TODO: need to check for ok, what should we do.....
	var stateA byte
	var stateB byte
	if isStatusPkt {
		if (status & 0x1) != 0 {
			stateA = 1
		}
		if (status & 0x2) != 0 {
			stateB = 1
		}
		//fmt.Printf("STATE_A: %d, STATE_B: %d\n", stateA, stateB)
	}
	return isStatusPkt, stateA, stateB, ok
}

func isStatusPacket(opcode byte) bool {
	return (opcode == 0x0A || opcode == 0xEF)
}

// TODO: This function belongs in a client lib until some of the discrepancies between upPacket and nodeRow are resolved
func packet2nodeState(p *message.UpPacket, nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6, markInvalid bool) {
	//nodeState.Eui.Set(p.Eui) // =		p.Eui
	nodeState.LastPktRcvd.Set(p.Data)
	nodeState.LastPktRcvdTs.Set(int64(p.Ts))
	nodeState.LastRssi.Set(p.Rssi)
	nodeState.LastSnr.Set(int(p.Snr * 10))
	if markInvalid == true {
		nodeState.ValidOnboard.Set(0)
	} else {
		nodeState.ValidOnboard.Set(1)
	}
	// The following need to be added to the structure
	//nodeRow.LastDr =	p.Dr
	//if p.Ack {
	//	nodeRow.LastAck = 1
	//}
	//nodeRow.LastCmd =	p.Cmd
	//nodeRow.LastFcnt =	p.Fcnt
	//nodeRow.LastFreq =	int64(p.Freq) // TODO: Is p.Freq too small to hold the freq?
	//nodeRow.LastPort =	p.Port
	//
}

// TODO: This needs to be tested
// TODO: There should be some logic that happens that check the length of the packet. That should happen
//	outside of this function and determine if the packet is valid or not.
func cloudSync2nodeState(p *message.UpPacket, nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6) {
	// TODO: Next to parse the packet and extract appNum, AppApi, and SysApi for ingestion
	loraPktSlice, _ := parse.HexStringToSlice(p.Data)
	// TODO: Need to determine if extracted values are valid. If not, then the record is not a valid onboard
	appNum := (int(loraPktSlice[3]) << 8) | int(loraPktSlice[4])
	sysApi := (int(loraPktSlice[1]) << 8) | int(loraPktSlice[2])
	appApi := (int(loraPktSlice[7]) << 8) | int(loraPktSlice[8])
	nodeState.AppNum.Set(appNum)
	nodeState.AppApi.Set(appApi)
	nodeState.SysApi.Set(sysApi)
	markInvalid := false // valid should depend on whether values above are
	packet2nodeState(p, nodeState, markInvalid)

}

// TODO: Need to parse packet and return bool
func isCloudSyncPkt(opcode byte) bool {
	return opcode == 0xFF
}
