package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/cornjacket/event_processor/app/controllers/responses"
)

func (app *AppContext) Info(w http.ResponseWriter, r *http.Request) {

	log.Println("The /info endpoint is being called...")
	responses.JSON(w, http.StatusOK, "Hello Consul Discovery")

}
