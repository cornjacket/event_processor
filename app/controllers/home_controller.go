package controllers

import (
	"net/http"

	"bitbucket.org/cornjacket/event_processor/app/controllers/responses"
)

func (app *AppContext) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Service Up")

}
