package controllers

import (
	"fmt"
	"log"
	"strings"

	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	kafka "github.com/segmentio/kafka-go"
)

// old code remove -- What does this message mean?
func getKafkaReader(kafkaURL, topic, groupID string) *kafka.Reader {
	brokers := strings.Split(kafkaURL, ",")
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers: brokers,
		GroupID: groupID,
		//Partition:  1,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}

// TODO(drt) - should this func return an error. What should happen based on this error?
// TODO(drt) - I think this function should be able to determine its own kafka String.
func (app *AppContext) createKafkaWorkerPool() {

	fmt.Println("init kafka consumer worker pool ... !!")
	topic := "cmd_event_packet"
	groupID := ""
	NWorkers := 100

	err := pktproc2.NewKafkaConsumerWorkerPool(app.KafkaURL, topic, groupID, NWorkers, app.packetHandler)
	if err != nil {
		fmt.Println("init pubsub error invoking NewKafkaConsumerWorkerPool ... !!")
	}

}

// TODO(drt) - should this func return an error. What should happen based on this error?
// TODO(drt) - if function does not return on error, then code fall through doesn't make sense.
// TODO(drt) - I think this function should be able to determine its own rabbitMQ info.
func (app *AppContext) createRabbitMQWorkerPool() {

	log.Println("init rabbitMQ consumer worker pool ... !!")
	groupID := ""
	NWorkers := 100

	// brokerAddress, queueName
	err := pktproc2.NewRabbitMQConsumerWorkerPool(app.BrokerAddress, "event-hndlr-packet", groupID, NWorkers, app.packetHandler)
	if err != nil {
		log.Println("init pubsub error invoking NewRabbitMQConsumerWorkerPool ... !!")
	}

}

/*
func (app *AppContext) createRabbitMQWorkerPool() {

	log.Println("createRabbitMQWorkerPool...!!")

	conn, err := amqp.Dial(app.BrokerAddress)
	if err != nil {
		log.Println("Failed to connect to RabbitMQ.")
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Println("Failed to open a channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"event-hndlr-packet", // name
		true,                 // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		log.Println("Failed to declare a queue")
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		log.Println("Failed to register a consumer")
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s\n", d.Body)

			// TODO: Need to implement a worker-pool to use with rabbitmq
			// Convert Body to message.UpPacket
			p := message.UpPacket{}
			// Populate the packet data
			json.Unmarshal(d.Body, &p) // TODO: There neds to be error handling here
			//app.packetHandler(wID int, p message.UpPacket) bool
			app.packetHandler(0, p)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func brokerAddr() string {
	brokerAddr := os.Getenv("EVENT_HNDLR_BROKER_ADDR")
	if len(brokerAddr) == 0 {
		brokerAddr = "amqp://guest:guest@localhost:5672/"
	}
	return brokerAddr
}
*/
