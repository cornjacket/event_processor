package controllers

import (
	// Standard library packets
	"fmt"
	"net/http"

	// Third party packages
	nodestate "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
	"github.com/julienschmidt/httprouter"
)

// A tallocrespproc.TallocResp is returned in the response
// swagger:response tallocrespResponse
type tallocrespResponseWrapper struct {
	// tallocresp send to Command Handler
	// in: body
	Body tallocrespproc.TallocResp
}

// swagger:route POST /tallocresp tallocresp sendTallocresp
// Sends a Tallocresp to the Event Handler. The Event Handler will process the Tallocresp and update
// its internal state.
// Returns Tallocresp to client
// responses:
//   201: tallocrespResponse

// This function returns the entry point for the tallocresp processing worker pool upon reception of
// a post to /tallocresp. The default behavior is for the tallocresp post worker to immediately queue the tallocresp
// followed by replying to the client with the posted tallocresp.
func (app *AppContext) NewTallocRespPoolEntryFunc(numWorkers int) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return tallocrespproc.NewResponseHandlerFunc(numWorkers, app.tallocRespHandler)

}

/* Taken from tallocresproc
type TallocResp struct {
        NodeEui         string
        Tslot           int
        Code            int // not sure how i will use this. maybe indicate whether valid or oversubscribed: >0 will be valid, otherwise error code
        Test            bool
        Ts              uint64
        CorrId  uint64  `json:"corrid"`
        ClassId string  `json:"classid"`
        Hops int        `json:"hops"`
}

*/

// Are there some system scenarios where the airtrafficcontrol would want the node controller to reset the node?
func (app *AppContext) tallocRespHandler(wID int, tresp tallocrespproc.TallocResp) bool {

	tallocrespproc.DisplayInfoMessageWithLatency("respnse rcvd from ATC", wID, &tresp)

	// TODO: parse the tallocResp, fetch the node row, and update the tslot and gateway EUI.
	// TODO: if there is no entry, then create one and fill with tallocresp values

	validTallocResp := validateTallocResp(&tresp) // Bring back the slice if it is needed somewhere
	if validTallocResp {

		err := fetchNodeStateUpdateTAlloc(&tresp)
		tallocrespproc.DisplayPassFailMessageWithLatency("fetch nodestate and update talloc", err, wID, &tresp, false)

		// FUTURE TODO: RespHandler should send the latest nodeState to the viewHandler so that it can cache it for viewing

	} else {
		tallocrespproc.DisplayInfoMessageWithLatency("invalid talloc resp rcvd. no action taken", wID, &tresp)

	}
	return true // this should be returning the error
}

// TODO: The are a few places where this function returns, it may be useful to return a code that indicates where the app terminated on error...
func fetchNodeStateUpdateTAlloc(tresp *tallocrespproc.TallocResp) error {

	// Fetch node state based on tresp's EUI, if not found, then we will automatically
	//    create the Node state row
	nodeState := nodestate.New()
	var nodeRow nodestate.Row

	nodeRow.Eui = tresp.NodeEui

	var found bool
	createRowIfNotFound := true
	lockRow := false                                                            // ATC correctly locks and unlocks. Review that code base and then revisit here.
	err, found := nodeState.GetByEui(nodeRow.Eui, createRowIfNotFound, lockRow) // TODO: The order of these return values are backwards. The generator needs to be fixed...
	if err != nil {
		fmt.Printf("tallocResp error: 1\n")
		return err
	}
	// if not found in node state table, then for now onboard as invalid
	if found == false {
		fmt.Printf("nodeState was not found: 1\n") // not very relevant. The controller will still allocate the tslot
	} else {
		// We should check the state of the node. If the node has completed cloudsync,
		// then if the current allocated tslot is different from the new tslot, then the node should be reset

		// TODO:
		fmt.Printf("nodeState was found: 1\n")
	}
	markInvalid := false // what is this based on?
	tallocResp2nodeState(tresp, nodeState, markInvalid)

	// Save node state
	_, err = nodeState.Update()
	if err != nil {
		fmt.Printf("tallcResp error: 2\n")
	}

	return err

}

func validateTallocResp(tresp *tallocrespproc.TallocResp) bool {
	if tresp.NodeEui == "" { // there should be a few general checks on the EUI, length, valid values, ..
		return false
	}
	if tresp.Tslot < 0 || tresp.Tslot > 59 { // the max value will need to change at some point
		return false
	}
	return true
}

// Note that this function does not use the GatewayEui info inside the TallocResp struct since nodestate does not contain that field.
// nodestate does not need to contain the gatewayEui because it does not receive GW packets. Therefore the GatewayEui field is not needed in
// the TallocResp field. It should be removed. 6/8/20
func tallocResp2nodeState(tresp *tallocrespproc.TallocResp, nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6, markInvalid bool) {

	if tresp.Code > 0 { // assume 0 or below is an error response to the service
		nodeState.TSlot.Set(tresp.Tslot)
		nodeState.TSlotValid.Set(1)
		nodeState.TSlotAssignedTs.Set(int64(tresp.Ts))
		// not sure about the logic in the following.
		if markInvalid == true {
			nodeState.ValidOnboard.Set(0)
		} else {
			nodeState.ValidOnboard.Set(1)
		}
	}
}

/*
func displayTallocResp(t *tallocrespproc.TallocResp, wID int, message string) {
	now := loriot.Now()
	latency := now - t.Ts
	fmt.Printf("INFO, classId: %s, corrId: %d, EUI: %s, code: %d, tslot: %d, worker %d, tallocResp: %s, hops: %d, ts: %d, ts0: %d, lat: %d\n",
                        t.ClassId, t.CorrId, t.NodeEui, t.Code, t.Tslot, wID, message, t.Hops, now, t.Ts, latency)
}
*/
