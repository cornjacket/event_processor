package mocks

import (
	"fmt"
	"log"

	"bitbucket.org/cornjacket/iot/message"
	view_models "bitbucket.org/cornjacket/view_hndlr/app/models"
)

// Should I rename this clientMock
type ViewHndlrClientMock struct {
}

var ViewLastRxPacket message.UpPacket
var ViewLastData view_models.Data
var ViewLastNodeState view_models.NodeState

func (s *ViewHndlrClientMock) Open() error {
	log.Println("ViewClientMock.Open() invoked.")
	return nil
}

func (s *ViewHndlrClientMock) SendPacket(packet message.UpPacket) error {
	log.Println("ViewClientMock.SendPacket() invoked.")
	ViewLastRxPacket = packet
	return nil
}

func (s *ViewHndlrClientMock) SendStatus(data view_models.Data) error {
	log.Println("ViewClientMock.SendStatus() invoked.")
	//fmt.Printf("ViewClientMock.SendStatus(): Data: %v\n", data)
	ViewLastData = data
	return nil
}

func (s *ViewHndlrClientMock) SendNodestate(nodestate *view_models.NodeState) error {
	//log.Println("ViewClientMock.SendNodestate() invoked.")
	fmt.Printf("ViewClientMock.SendSNodestate(): ActualA: %d, ActualB: %d\n", nodestate.ActualStateA, nodestate.ActualStateB)
	ViewLastNodeState = *nodestate
	return nil
}
