package mocks

import (
	//"fmt"
	"log"
	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client_lib"
)

// Should I rename this clientMock
type AirTrafficControlClientMock struct {
}

var AtcLastTallocReq	atc_client.TAllocReq

func (s *AirTrafficControlClientMock) Open() error {
	log.Println("AtcClientMock.Open() invoked.")
	AtcLastTallocReq = atc_client.TAllocReq{}
	return nil
}

func (s *AirTrafficControlClientMock) RequestTslot(tallocreq *atc_client.TAllocReq) (error) {
	log.Println("AtcClientMock.RequestTslot() invoked.")
	AtcLastTallocReq = *tallocreq 
	return nil
}
