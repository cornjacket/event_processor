package app

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"bitbucket.org/cornjacket/discovery"
	"bitbucket.org/cornjacket/event_processor/app/controllers"
	nodestate "bitbucket.org/cornjacket/event_processor/app/repos/nodestate_appnum4appapi3sysapi6"
	packet "bitbucket.org/cornjacket/event_processor/app/repos/packet_appnum4appapi3sysapi6"
	"bitbucket.org/cornjacket/event_processor/app/utils/database"
	"bitbucket.org/cornjacket/event_processor/app/utils/env"
)

type AppService struct {
	Env     AppEnv
	Context *controllers.AppContext
	Db      *sql.DB // not sure how i am going to use since this is maintained inside the package libs. can use for closing the db when testing.
}

type AppEnv struct {
	DbPassword string
	DbHostname string
	DbUsername string
	DbPort     string
	DbName     string
	PortNum    string
}

func NewAppService() AppService {
	a := AppService{}
	a.Context = controllers.NewAppContext()
	return a
}

func (a *AppService) InitEnv() {

	a.Env.DbPassword = env.GetVar("DB_PASSWORD", "", true)
	a.Env.DbHostname = env.GetVar("DB_HOSTNAME", "localhost", false)
	a.Env.DbUsername = env.GetVar("DB_USERNAME", "root", false)
	a.Env.DbPort = env.GetVar("DB_PORT", "3306", false)
	a.Env.DbName = env.GetVar("DB_NAME", "test", false)
	a.Env.PortNum = env.GetVar("EVENT_PORT_NUM", "8081", false)
}

// TODO(drt) - check how kafka is used. i believe it is only used packet path for both consumer and producer.
// Not sure how kafka and http will co-exist. Code is in a hybrid stage supporting both at the moment.
func (a *AppService) SetupExternalServices() {

	// NOTE: 10/31/20 But what if kafka is provided. then what should happen. right now there is a hybrid of using kafka and http
	// i think that the event_processor only really needs to use kafka. however for local testing http is used.
	// so, then if kakfa is used, then consul should not be used for the event-hndlr.
	// note that the nodestate and data paths have not been implemented in kafka yet.

// TODO: The following code is not necessary since the ATC client code takes care of the env variables 

	if a.Context == nil {
		log.Fatal("AppService.Context == nil. Quitting\n")
	}
	if err := a.Context.ViewHndlrClient.Open(); err != nil {
		log.Println("EventHndrlService.Open() error: ", err)
	}
	if err := a.Context.AirTrafficControlService.Open(); err != nil {
		log.Println("AirTrafficControlService.Open() error: ", err)
	}

}

func (a *AppService) Init() {
	a.InitEnv()
	a.DropTables()
	a.SetupDatabase()
	a.SetupExternalServices()
	discovery.RegisterService("event-hndlr", a.Env.PortNum)
}

func (a *AppService) Run() {

	checkIfProjectNeedsBuild() // will this work anymore now that build file will be in subfolder?
	if a.Context == nil {
		log.Fatal("AppService.Context == nil. Quitting\n")
	}
	a.Context.Run(a.Env.PortNum)

}

// Following function is for test purposes only
func (a *AppService) DropTables() {

	fmt.Printf("-------------------------- Tables dropped invoked.\n")
	tableName := nodestate.New().GetCurrentTableName()
	err := database.DropTableIfExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, tableName)
	if err != nil {
		fmt.Printf("init\tdropTable() failed: %s\n", tableName)
		panic(err.Error())
	}
	tableName = packet.New().GetCurrentTableName()
	err = database.DropTableIfExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, tableName)
	if err != nil {
		fmt.Printf("init\tdropTable() failed: %s\n", tableName)
		panic(err.Error())
	}
}

func (a *AppService) SetupDatabase() {

	err := database.CreateDatabaseIfNotExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort)
	if err != nil {
		fmt.Printf("init\tcreateDatabase() failed: %s\n", a.Env.DbName)
		panic(err.Error())
	}

	dbConnString := a.Env.DbUsername + ":XXXX" + "@tcp(" + a.Env.DbHostname + ":" + a.Env.DbPort + ")/" + a.Env.DbName + "?parseTime=true"
	//fmt.Printf("init\tdebug: %s\n", dbConnString)
	dbConnString = a.Env.DbUsername + ":" + a.Env.DbPassword + "@tcp(" + a.Env.DbHostname + ":" + a.Env.DbPort + ")/" + a.Env.DbName + "?parseTime=true"

	a.Db, err = sql.Open("mysql", dbConnString)
	if err != nil {
		panic(err.Error())
	}
	a.CreateTables() // TODO(drt) What about err return value?
}

// TEST function - side effect of Init functions is to create the table if it doesn't already exist. This should be used for component testing
// TODO(drt) - these init functions need to return an error that can be checked.
// Init will also seet the internal package level db_conn variable that each database package uses..
func (a *AppService) CreateTables() {
	fmt.Printf("-------------------------- Tables created invoked.\n")
	nodestate.Init(a.Db)
	packet.Init(a.Db)
}

func (a *AppService) GetAirTrafficControlHostnamePort() (string, string) {
	var host, port string
	if discovery.UrlProvided() {
		// TODO(drt) what about error handling in case system failed
		host, port = discovery.LookupService("air-traffic-control")
	} else {
		host = env.GetVar("ATC_HOSTNAME", "localhost", false)
		port = env.GetVar("ATC_PORT", "8083", false)
	}
	return host, port
}

func checkIfProjectNeedsBuild() {
	if _, err := os.Stat("./build.json"); err == nil {
		fmt.Println("Project config has changed but build was not invoked. Run \"guild Build\"")
		os.Exit(1)
	}
}
