package pinger 

import (
	"database/sql"
	"log"
	"time"

	//"github.com/hashicorp/vault/api" // DRT - removed since not going to use
)

type Pinger interface {
	Ping() error
}

type DbPinger struct {
	Db *sql.DB
}

func (d DbPinger) Ping() error {
	return d.Db.Ping()
}

// pingExternalService pings an external service with linearly increasing backoff time.
func PingExternalService(addr string, pinger Pinger) error {
	numBackOffIterations := 15
	for i := 1; i <= numBackOffIterations; i++ {
		log.Printf("Pinging %s.\n", addr)
		err := pinger.Ping()
		if err != nil {
			log.Println(err)
		}
		if err == nil {
			log.Printf("Connected to %s.", addr)
			break
		}
		waitDuration := time.Duration(i) * time.Second
		log.Printf("Backing off for %v.\n", waitDuration)
		time.Sleep(waitDuration)
		if i == numBackOffIterations {
			return err
		}
	}
	return nil
}

/*
type vaultPinger struct {
	vaultClient *api.Client
	path        string
}

func (v *vaultPinger) ping() error {
	_, err := v.vaultClient.Logical().Read(v.path)
	return err
}

type vaultAppRolePinger struct {
	vaultClient *api.Client
	path        string
	options     map[string]interface{}
}

func (v *vaultAppRolePinger) ping() error {
	_, err := v.vaultClient.Logical().Write(v.path, v.options)
	return err
}
*/
