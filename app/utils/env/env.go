package env 

import (
	"fmt"
	"log"
	"os"
)

// purpose is to retrieve an environment variable from the os and if not defined return a default value
// if the variable must be defined for correct operation, then the program will terminate
func GetVar(envName string, defaultEnvValue string, required bool) (envValue string) {
	envValue = os.Getenv(envName)
	if envValue == "" {
		if required {
			log.Fatalf("Error %s env variable was not provided.", envName)
		}
		envValue = defaultEnvValue 
	}
	fmt.Printf("%s: %s\n", envName, envValue)
	return envValue
}
