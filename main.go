package main

import (
	"bitbucket.org/cornjacket/event_processor/app"
)

func main() {

	eventApp := app.NewAppService()	
	eventApp.Init()
	eventApp.Run()

}

